﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Calib" Type="Folder" URL="../Calib">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="RT PXI Target" Type="RT PXI Chassis">
		<Property Name="alias.name" Type="Str">RT PXI Target</Property>
		<Property Name="alias.value" Type="Str">172.18.48.242</Property>
		<Property Name="CCSymbols" Type="Str">TARGET_TYPE,RT;OS,PharLap;CPU,x86;</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">3</Property>
		<Property Name="host.TargetOSID" Type="UInt">15</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/AFP_RP.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">true</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/c/ni-rt/startup</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str">Listen 8000

NI.ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
DirectoryIndex index.htm
WorkerLimit 10
InactivityTimeout 60

LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule

#
# Pipeline Definition
#

SetConnector netConnector

AddHandler LVAuth
AddHandler LVRFP

AddHandler fileHandler ""

AddOutputFilter chunkFilter


</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="lib" Type="Folder" URL="../lib">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="MotorControl" Type="FPGA Target">
			<Property Name="AutoRun" Type="Bool">false</Property>
			<Property Name="configString.guid" Type="Str">{0556D37E-90B9-4E1C-9500-E9D99FC0470D}resource=/crio_Chassis1_Mod1/DI21;0;ReadMethodType=bool{05B167CA-74CA-4A1F-B6F3-1433B8C464B0}resource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool{06B03385-1198-4227-9EE1-1A68F6A66CF6}resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=bool{092B54D3-F319-44DF-B5C5-FB6C65250EF6}resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=bool{0A556816-8B06-4BD7-8E2B-BBA4AA9941F6}resource=/crio_Chassis1_Mod1/DI0;0;ReadMethodType=bool{0B7B93DC-F81A-4F27-BF0A-0173A6C65A56}resource=/crio_Chassis1_Mod1/DI18;0;ReadMethodType=bool{0C5A6A27-AFE2-47B3-BEF8-ABEF68CDF6AD}resource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool{0D6468E2-3FD5-4759-8248-2A0DCCDE7883}resource=/crio_Chassis1_Mod1/DI28;0;ReadMethodType=bool{107ACC86-B407-4149-9CC0-5513DCBEAEB8}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{11809FA0-432E-4CB3-8955-6FB1161A4171}resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=bool{11D0A347-B69B-46E4-ADC4-111D1CBB2726}resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=bool{1DD3F40A-58E6-4AC9-AA0D-382E0A4F0C96}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{24554BDC-6DAC-40EE-A7E4-A91DBF460EEC}resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool{25CB0153-BB77-4844-8B4D-D0C3612B4DD7}resource=/crio_Chassis1_Mod1/DI29;0;ReadMethodType=bool{289B4871-4F53-42C5-A2C7-5513ABDF69ED}resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=bool{3384035F-C4D0-4CA2-83C7-018907E1504E}resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=bool{35132869-D22F-4F18-9935-930332E0FC22}resource=/crio_Chassis1_Mod1/DI17;0;ReadMethodType=bool{356EE839-06ED-48FE-967D-1B7E727F9E6E}resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=bool{3570E1B0-405C-4FC1-BC50-507AEEC433E6}resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=bool{394F3452-21A5-4B7A-96FB-D7DDDA8BE291}resource=/crio_Chassis1_Mod1/DI23;0;ReadMethodType=bool{3C8CBFF7-B97D-4A2E-B47C-5F26D74576E3}resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=bool{3F65A59B-EE0A-4EB6-8D2F-C8901898DA81}resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=bool{43B9D847-69FF-4D49-B8E2-C185DBA2E33B}resource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=bool{4537DE5C-CD24-4B92-8154-7E72D5412F57}resource=/crio_Chassis1_Mod3/DI23;0;ReadMethodType=bool{499FFC76-8829-4838-BFAC-4213C896EE6E}resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=bool{49A6EEE5-22D4-4F71-8846-2A3F8DE54B0D}resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=bool{4B538F5A-A88F-40DC-B68F-05D9EAB976FF}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{5143860F-7E77-4B0E-B0FA-618983E8BF75}resource=/crio_Chassis1_Mod1/DI5;0;ReadMethodType=bool{54348C86-CD9E-4CDD-8086-0744670327EB}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{55C8C287-76AD-433C-AC8A-A4D4ECD9D82F}resource=/crio_Chassis1_Mod1/DI12;0;ReadMethodType=bool{55FE2007-EC58-4390-B09C-50E4CFDC84F6}resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=bool{595D7BDB-CE99-4B33-884D-A449DED7DF1F}resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=bool{5AFE7AC4-2AEB-48FF-9427-906EA6DCCDB8}resource=/crio_Chassis1_Mod1/DI11;0;ReadMethodType=bool{5D7F7C8A-D26B-485C-AF42-ABC9E484BF6A}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{5D9F73EB-38B9-41E4-A321-BC35A536B5F2}resource=/crio_Chassis1_Mod3/DI20;0;ReadMethodType=bool{61438BB2-0BEF-43B8-BB66-958D9E5E58B3}resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=bool{624B770E-F680-46DD-8B3E-7906D07D694E}resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=bool{63562E57-1DB7-487A-BBAE-BEC1EA804336}resource=/crio_Chassis1_Mod1/DI13;0;ReadMethodType=bool{6C418BAC-2150-46F7-8E7E-B62F91EF7EE2}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{790B0AF0-14E3-4353-B54F-E4D0514831B1}resource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=bool{79E8A5A9-D2D8-4616-9BDB-EAB5DA01BA73}resource=/crio_Chassis1_Mod1/DI2;0;ReadMethodType=bool{7A2528D8-D4F3-44ED-8D04-CF7636939773}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{7C303257-81DB-4EB4-B16D-A4C407320085}resource=/crio_Chassis1_Mod1/DI7;0;ReadMethodType=bool{7CE7CFCA-B9BA-45F3-BB44-6CC1D0DEE941}resource=/crio_Chassis1_Mod3/DI21;0;ReadMethodType=bool{7F9A7EB3-C747-48B8-B0D0-8259813C9C37}resource=/crio_Chassis1_Mod1/DI15;0;ReadMethodType=bool{84AEB81F-79DD-4B98-8923-9A31C8392FCE}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{84B346B4-F091-4B7A-B4EB-64A001D358F0}resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=bool{85E5E6F2-E926-4D6F-9125-6B879BB88DDE}resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=bool{86B8A4AF-9DF6-48E1-8583-F555902F2935}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{86D327ED-BA4F-4823-98AC-D30E08C4501B}resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=bool{8717D8EB-042E-4EC6-A1DE-21BFBBBF26B3}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{87FB5AE6-C649-45E5-82FF-E05CA0DA03F3}resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=bool{8809A15E-611D-4E98-BFCB-C66B7930EEFD}resource=/crio_Chassis1_Mod1/DI3;0;ReadMethodType=bool{883EB41D-3391-44E9-87CB-6A5836B5E327}resource=/crio_Chassis1_Mod1/DI14;0;ReadMethodType=bool{88BB7CCE-977D-457E-BC2C-2D6DCA928E88}resource=/crio_Chassis1_Mod1/DI26;0;ReadMethodType=bool{8976858B-11B3-44C9-A583-D568A0CF0B54}resource=/crio_Chassis1_Mod1/DI6;0;ReadMethodType=bool{8D762FB0-711D-40D6-B3D3-425DA7DAB7E5}resource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=bool{992F089D-7544-40C6-9C1A-BE030D2F152D}resource=/crio_Chassis1_Mod1/DI10;0;ReadMethodType=bool{9AAC76B7-21CB-4252-AAAA-98006C6B2BA6}resource=/crio_Chassis1_Mod3/DI22;0;ReadMethodType=bool{9F63FA0C-E844-4960-BD9C-5F4BFABD6505}resource=/crio_Chassis1_Mod1/DI30;0;ReadMethodType=bool{9FCB72D8-E1B6-4D17-8DEF-116D9AE3504D}resource=/crio_Chassis1_Mod1/DI24;0;ReadMethodType=bool{A2C4AF18-4DAC-48C3-B958-0DD9A2074C70}resource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=bool{A5825855-A5C7-4786-8A83-4DFE22D2804C}resource=/crio_Chassis1_Mod1/DI16;0;ReadMethodType=bool{A5EC982B-052F-4529-A461-958B89D01CDF}resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=bool{A604AFC3-6806-4388-BD36-F8325FAB8319}resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool{AA7AAD24-9C05-4E75-A2E8-981B8C1E2C17}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{AF72DB20-FDCA-4063-A278-74A77EB04F3E}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{B0B80628-F030-429D-AACC-9311D9CFAE8D}resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=bool{B250CE91-D2AD-411C-8048-717C84C843B6}resource=/crio_Chassis1_Mod1/DI22;0;ReadMethodType=bool{BBFBE534-752C-4B05-A58A-74FBA33176AC}resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=bool{BDE238D6-A11D-4DCA-8214-A0458FC9206B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{C224D373-096C-4B09-AC7E-0D3BD9F4B004}resource=/crio_Chassis1_Mod1/DI19;0;ReadMethodType=bool{C75EB28A-4E67-4AD7-83A0-DE2B3ACAAEDC}resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=bool{CB73F8B6-4B01-43F5-8A39-0636F7D0D425}resource=/crio_Chassis1_Mod1/DI27;0;ReadMethodType=bool{CBF7C3CB-8CDF-479C-8B56-50DE4364D918}resource=/crio_Chassis1_Mod1/DI25;0;ReadMethodType=bool{CDBB159E-172E-4042-B68F-FE23137470FF}resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool{CF744ECD-633C-4E9C-B975-16468E74E5AB}resource=/crio_Chassis1_Mod1/DI31;0;ReadMethodType=bool{D79F9C1E-1133-4E0D-B0AF-C5F95845EB93}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{D92C418B-6E9B-4996-92AA-5E8886F4AE74}resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=bool{D96E4E8B-67CF-48AB-ABE7-D7C631C60ED1}resource=/crio_Chassis1_Mod1/DI9;0;ReadMethodType=bool{D977F5E5-72D5-4664-B839-CEC8E13AE14E}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{DA63B211-9107-4251-A772-48367B9740E0}resource=/crio_Chassis1_Mod1/DI1;0;ReadMethodType=bool{E45A28CB-E645-47DC-A6E0-108A37CB0822}resource=/crio_Chassis1_Mod1/DI4;0;ReadMethodType=bool{E5C640DD-24F1-491E-AA65-289EBE3C8647}resource=/crio_Chassis1_Mod1/DI8;0;ReadMethodType=bool{EFEEFF89-A53D-4578-874F-57741729690B}resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=bool{F9105A72-F1D0-4E0E-9228-EE51EDD742CA}resource=/crio_Chassis1_Mod1/DI20;0;ReadMethodType=bool{F926388E-E270-4107-A427-4597EACE1607}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{FF7CAAE0-2B12-4DED-8FED-B66258DA1B54}resource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="configString.name" Type="Str">24V_Ok_M01resource=/crio_Chassis1_Mod1/DI0;0;ReadMethodType=bool24V_Ok_M02resource=/crio_Chassis1_Mod1/DI1;0;ReadMethodType=bool24V_Ok_M03resource=/crio_Chassis1_Mod1/DI2;0;ReadMethodType=bool24V_Ok_M04resource=/crio_Chassis1_Mod1/DI3;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;All_RP_Out_1resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=boolAll_RP_Out_2resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=boolChassis1_Mod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Copy_HomeSw_M01resource=/crio_Chassis1_Mod3/DI20;0;ReadMethodType=boolCopy_HomeSw_M02resource=/crio_Chassis1_Mod3/DI21;0;ReadMethodType=boolCopy_HomeSw_M03resource=/crio_Chassis1_Mod3/DI22;0;ReadMethodType=boolCopy_HomeSw_M04resource=/crio_Chassis1_Mod3/DI23;0;ReadMethodType=boolDevice_Allowedresource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=boolDirection_M01resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=boolDirection_M02resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=boolDirection_M03resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=boolDirection_M04resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=boolDriverFlt_M01resource=/crio_Chassis1_Mod1/DI20;0;ReadMethodType=boolDriverFlt_M02resource=/crio_Chassis1_Mod1/DI21;0;ReadMethodType=boolDriverFlt_M03resource=/crio_Chassis1_Mod1/DI22;0;ReadMethodType=boolDriverFlt_M04resource=/crio_Chassis1_Mod1/DI23;0;ReadMethodType=boolEnable_M01resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=boolEnable_M02resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=boolEnable_M03resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=boolEnable_M04resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=boolImminent_Beam_Abortresource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=boolInjection_Inhibit_1resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=boolInjection_Inhibit_2resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=boolLVDT_Flt_Copy_M01resource=/crio_Chassis1_Mod1/DI25;0;ReadMethodType=boolLVDT_Flt_Copy_M02resource=/crio_Chassis1_Mod1/DI27;0;ReadMethodType=boolLVDT_Flt_Copy_M03resource=/crio_Chassis1_Mod1/DI29;0;ReadMethodType=boolLVDT_Flt_Copy_M04resource=/crio_Chassis1_Mod1/DI31;0;ReadMethodType=boolMotor_Driver_Offresource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=boolMvt_inhibitresource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=boolNot_Back_Homeresource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=boolOutOfLimits_Copy_M01resource=/crio_Chassis1_Mod1/DI24;0;ReadMethodType=boolOutOfLimits_Copy_M02resource=/crio_Chassis1_Mod1/DI26;0;ReadMethodType=boolOutOfLimits_Copy_M03resource=/crio_Chassis1_Mod1/DI28;0;ReadMethodType=boolOutOfLimits_Copy_M04resource=/crio_Chassis1_Mod1/DI30;0;ReadMethodType=boolOverride_1resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=boolOverride_2resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Reset_M01resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=boolReset_M02resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=boolReset_M03resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=boolReset_M04resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=boolResolver_Cos_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16Resolver_Cos_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16Resolver_Cos_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16Resolver_Cos_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Resolver_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16Resolver_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16Resolver_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16Resolver_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16Resolver_Sin_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16Resolver_Sin_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16Resolver_Sin_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16Resolver_Sin_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16Safe_Beam_1resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=boolSafe_Beam_2resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=boolSpare_1resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=boolSpare_2resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=boolStable_Beamresource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=boolStep_M01resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=boolStep_M02resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=boolStep_M03resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=boolStep_M04resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=boolStopper_In_M01resource=/crio_Chassis1_Mod1/DI7;0;ReadMethodType=boolStopper_In_M02resource=/crio_Chassis1_Mod1/DI11;0;ReadMethodType=boolStopper_In_M03resource=/crio_Chassis1_Mod1/DI15;0;ReadMethodType=boolStopper_In_M04resource=/crio_Chassis1_Mod1/DI19;0;ReadMethodType=boolStopper_Out_M01resource=/crio_Chassis1_Mod1/DI6;0;ReadMethodType=boolStopper_Out_M02resource=/crio_Chassis1_Mod1/DI10;0;ReadMethodType=boolStopper_Out_M03resource=/crio_Chassis1_Mod1/DI14;0;ReadMethodType=boolStopper_Out_M04resource=/crio_Chassis1_Mod1/DI18;0;ReadMethodType=boolSw_In_M01resource=/crio_Chassis1_Mod1/DI5;0;ReadMethodType=boolSw_In_M02resource=/crio_Chassis1_Mod1/DI9;0;ReadMethodType=boolSw_In_M03resource=/crio_Chassis1_Mod1/DI13;0;ReadMethodType=boolSw_In_M04resource=/crio_Chassis1_Mod1/DI17;0;ReadMethodType=boolSw_Out_M01resource=/crio_Chassis1_Mod1/DI4;0;ReadMethodType=boolSw_Out_M02resource=/crio_Chassis1_Mod1/DI8;0;ReadMethodType=boolSw_Out_M03resource=/crio_Chassis1_Mod1/DI12;0;ReadMethodType=boolSw_Out_M04resource=/crio_Chassis1_Mod1/DI16;0;ReadMethodType=boolUser_Permit_1aresource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=boolUser_Permit_1bresource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=boolUser_Permit_2aresource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=boolUser_Permit_2bresource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool</Property>
			<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">PXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA</Property>
			<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
			<Property Name="Resource Name" Type="Str">RIO0</Property>
			<Property Name="Target Class" Type="Str">PXI-7852R</Property>
			<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
			<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
			<Item Name="Chassis1_Mod1_9425DI" Type="Folder">
				<Item Name="24V_Ok_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0A556816-8B06-4BD7-8E2B-BBA4AA9941F6}</Property>
				</Item>
				<Item Name="24V_Ok_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DA63B211-9107-4251-A772-48367B9740E0}</Property>
				</Item>
				<Item Name="24V_Ok_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{79E8A5A9-D2D8-4616-9BDB-EAB5DA01BA73}</Property>
				</Item>
				<Item Name="24V_Ok_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8809A15E-611D-4E98-BFCB-C66B7930EEFD}</Property>
				</Item>
				<Item Name="Sw_Out_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E45A28CB-E645-47DC-A6E0-108A37CB0822}</Property>
				</Item>
				<Item Name="Sw_In_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5143860F-7E77-4B0E-B0FA-618983E8BF75}</Property>
				</Item>
				<Item Name="Stopper_Out_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8976858B-11B3-44C9-A583-D568A0CF0B54}</Property>
				</Item>
				<Item Name="Stopper_In_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7C303257-81DB-4EB4-B16D-A4C407320085}</Property>
				</Item>
				<Item Name="Sw_Out_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E5C640DD-24F1-491E-AA65-289EBE3C8647}</Property>
				</Item>
				<Item Name="Sw_In_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI9</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D96E4E8B-67CF-48AB-ABE7-D7C631C60ED1}</Property>
				</Item>
				<Item Name="Stopper_Out_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI10</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{992F089D-7544-40C6-9C1A-BE030D2F152D}</Property>
				</Item>
				<Item Name="Stopper_In_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI11</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5AFE7AC4-2AEB-48FF-9427-906EA6DCCDB8}</Property>
				</Item>
				<Item Name="Sw_Out_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI12</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{55C8C287-76AD-433C-AC8A-A4D4ECD9D82F}</Property>
				</Item>
				<Item Name="Sw_In_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI13</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{63562E57-1DB7-487A-BBAE-BEC1EA804336}</Property>
				</Item>
				<Item Name="Stopper_Out_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI14</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{883EB41D-3391-44E9-87CB-6A5836B5E327}</Property>
				</Item>
				<Item Name="Stopper_In_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI15</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7F9A7EB3-C747-48B8-B0D0-8259813C9C37}</Property>
				</Item>
				<Item Name="Sw_Out_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A5825855-A5C7-4786-8A83-4DFE22D2804C}</Property>
				</Item>
				<Item Name="Sw_In_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI17</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{35132869-D22F-4F18-9935-930332E0FC22}</Property>
				</Item>
				<Item Name="Stopper_Out_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI18</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0B7B93DC-F81A-4F27-BF0A-0173A6C65A56}</Property>
				</Item>
				<Item Name="Stopper_In_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI19</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C224D373-096C-4B09-AC7E-0D3BD9F4B004}</Property>
				</Item>
				<Item Name="DriverFlt_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI20</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F9105A72-F1D0-4E0E-9228-EE51EDD742CA}</Property>
				</Item>
				<Item Name="DriverFlt_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI21</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0556D37E-90B9-4E1C-9500-E9D99FC0470D}</Property>
				</Item>
				<Item Name="DriverFlt_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI22</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B250CE91-D2AD-411C-8048-717C84C843B6}</Property>
				</Item>
				<Item Name="DriverFlt_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI23</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{394F3452-21A5-4B7A-96FB-D7DDDA8BE291}</Property>
				</Item>
				<Item Name="OutOfLimits_Copy_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9FCB72D8-E1B6-4D17-8DEF-116D9AE3504D}</Property>
				</Item>
				<Item Name="LVDT_Flt_Copy_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI25</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CBF7C3CB-8CDF-479C-8B56-50DE4364D918}</Property>
				</Item>
				<Item Name="OutOfLimits_Copy_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI26</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{88BB7CCE-977D-457E-BC2C-2D6DCA928E88}</Property>
				</Item>
				<Item Name="LVDT_Flt_Copy_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI27</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CB73F8B6-4B01-43F5-8A39-0636F7D0D425}</Property>
				</Item>
				<Item Name="OutOfLimits_Copy_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI28</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0D6468E2-3FD5-4759-8248-2A0DCCDE7883}</Property>
				</Item>
				<Item Name="LVDT_Flt_Copy_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI29</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{25CB0153-BB77-4844-8B4D-D0C3612B4DD7}</Property>
				</Item>
				<Item Name="OutOfLimits_Copy_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI30</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9F63FA0C-E844-4960-BD9C-5F4BFABD6505}</Property>
				</Item>
				<Item Name="LVDT_Flt_Copy_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DI31</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CF744ECD-633C-4E9C-B975-16468E74E5AB}</Property>
				</Item>
			</Item>
			<Item Name="Chassis1_Mod2_9477DO" Type="Folder">
				<Item Name="Step_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{61438BB2-0BEF-43B8-BB66-958D9E5E58B3}</Property>
				</Item>
				<Item Name="Direction_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{092B54D3-F319-44DF-B5C5-FB6C65250EF6}</Property>
				</Item>
				<Item Name="Enable_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{624B770E-F680-46DD-8B3E-7906D07D694E}</Property>
				</Item>
				<Item Name="Reset_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{86D327ED-BA4F-4823-98AC-D30E08C4501B}</Property>
				</Item>
				<Item Name="Step_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{55FE2007-EC58-4390-B09C-50E4CFDC84F6}</Property>
				</Item>
				<Item Name="Direction_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3C8CBFF7-B97D-4A2E-B47C-5F26D74576E3}</Property>
				</Item>
				<Item Name="Enable_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{356EE839-06ED-48FE-967D-1B7E727F9E6E}</Property>
				</Item>
				<Item Name="Reset_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{11809FA0-432E-4CB3-8955-6FB1161A4171}</Property>
				</Item>
				<Item Name="Step_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BBFBE534-752C-4B05-A58A-74FBA33176AC}</Property>
				</Item>
				<Item Name="Direction_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO9</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C75EB28A-4E67-4AD7-83A0-DE2B3ACAAEDC}</Property>
				</Item>
				<Item Name="Enable_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO10</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{11D0A347-B69B-46E4-ADC4-111D1CBB2726}</Property>
				</Item>
				<Item Name="Reset_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO11</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{85E5E6F2-E926-4D6F-9125-6B879BB88DDE}</Property>
				</Item>
				<Item Name="Step_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO12</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D92C418B-6E9B-4996-92AA-5E8886F4AE74}</Property>
				</Item>
				<Item Name="Direction_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO13</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{49A6EEE5-22D4-4F71-8846-2A3F8DE54B0D}</Property>
				</Item>
				<Item Name="Enable_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO14</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{595D7BDB-CE99-4B33-884D-A449DED7DF1F}</Property>
				</Item>
				<Item Name="Reset_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO15</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{84B346B4-F091-4B7A-B4EB-64A001D358F0}</Property>
				</Item>
			</Item>
			<Item Name="Chassis1_Mod3" Type="Folder">
				<Item Name="Device_Allowed" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0C5A6A27-AFE2-47B3-BEF8-ABEF68CDF6AD}</Property>
				</Item>
				<Item Name="Stable_Beam" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A2C4AF18-4DAC-48C3-B958-0DD9A2074C70}</Property>
				</Item>
				<Item Name="Safe_Beam_1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{87FB5AE6-C649-45E5-82FF-E05CA0DA03F3}</Property>
				</Item>
				<Item Name="Safe_Beam_2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B0B80628-F030-429D-AACC-9311D9CFAE8D}</Property>
				</Item>
				<Item Name="Motor_Driver_Off" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{289B4871-4F53-42C5-A2C7-5513ABDF69ED}</Property>
				</Item>
				<Item Name="User_Permit_1a" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{43B9D847-69FF-4D49-B8E2-C185DBA2E33B}</Property>
				</Item>
				<Item Name="User_Permit_1b" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8D762FB0-711D-40D6-B3D3-425DA7DAB7E5}</Property>
				</Item>
				<Item Name="Injection_Inhibit_1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{499FFC76-8829-4838-BFAC-4213C896EE6E}</Property>
				</Item>
				<Item Name="Injection_Inhibit_2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{EFEEFF89-A53D-4578-874F-57741729690B}</Property>
				</Item>
				<Item Name="User_Permit_2a" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI9</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A5EC982B-052F-4529-A461-958B89D01CDF}</Property>
				</Item>
				<Item Name="User_Permit_2b" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI10</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{05B167CA-74CA-4A1F-B6F3-1433B8C464B0}</Property>
				</Item>
				<Item Name="All_RP_Out_1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI11</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CDBB159E-172E-4042-B68F-FE23137470FF}</Property>
				</Item>
				<Item Name="All_RP_Out_2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI12</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3384035F-C4D0-4CA2-83C7-018907E1504E}</Property>
				</Item>
				<Item Name="Not_Back_Home" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI13</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3570E1B0-405C-4FC1-BC50-507AEEC433E6}</Property>
				</Item>
				<Item Name="Override_1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI14</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{06B03385-1198-4227-9EE1-1A68F6A66CF6}</Property>
				</Item>
				<Item Name="Override_2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI15</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3F65A59B-EE0A-4EB6-8D2F-C8901898DA81}</Property>
				</Item>
				<Item Name="Mvt_inhibit" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{790B0AF0-14E3-4353-B54F-E4D0514831B1}</Property>
				</Item>
				<Item Name="Imminent_Beam_Abort" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI17</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FF7CAAE0-2B12-4DED-8FED-B66258DA1B54}</Property>
				</Item>
				<Item Name="Spare_1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI18</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{24554BDC-6DAC-40EE-A7E4-A91DBF460EEC}</Property>
				</Item>
				<Item Name="Spare_2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI19</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A604AFC3-6806-4388-BD36-F8325FAB8319}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI20</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5D9F73EB-38B9-41E4-A321-BC35A536B5F2}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI21</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7CE7CFCA-B9BA-45F3-BB44-6CC1D0DEE941}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI22</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9AAC76B7-21CB-4252-AAAA-98006C6B2BA6}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI23</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{4537DE5C-CD24-4B92-8154-7E72D5412F57}</Property>
				</Item>
			</Item>
			<Item Name="Connector0" Type="Folder">
				<Item Name="Resolver_Cos_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8717D8EB-042E-4EC6-A1DE-21BFBBBF26B3}</Property>
				</Item>
				<Item Name="Resolver_Sin_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{54348C86-CD9E-4CDD-8086-0744670327EB}</Property>
				</Item>
				<Item Name="Resolver_Cos_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{4B538F5A-A88F-40DC-B68F-05D9EAB976FF}</Property>
				</Item>
				<Item Name="Resolver_Sin_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D79F9C1E-1133-4E0D-B0AF-C5F95845EB93}</Property>
				</Item>
				<Item Name="Resolver_Cos_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6C418BAC-2150-46F7-8E7E-B62F91EF7EE2}</Property>
				</Item>
				<Item Name="Resolver_Sin_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{84AEB81F-79DD-4B98-8923-9A31C8392FCE}</Property>
				</Item>
				<Item Name="Resolver_Cos_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{86B8A4AF-9DF6-48E1-8583-F555902F2935}</Property>
				</Item>
				<Item Name="Resolver_Sin_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D977F5E5-72D5-4664-B839-CEC8E13AE14E}</Property>
				</Item>
				<Item Name="Resolver_Exc_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1DD3F40A-58E6-4AC9-AA0D-382E0A4F0C96}</Property>
				</Item>
				<Item Name="Resolver_Exc_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7A2528D8-D4F3-44ED-8D04-CF7636939773}</Property>
				</Item>
				<Item Name="Resolver_Exc_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BDE238D6-A11D-4DCA-8214-A0458FC9206B}</Property>
				</Item>
				<Item Name="Resolver_Exc_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F926388E-E270-4107-A427-4597EACE1607}</Property>
				</Item>
			</Item>
			<Item Name="StateMachine" Type="Folder">
				<Item Name="M01_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../fpga/Motor/M01_ALFA_RP_State.lvsc"/>
				<Item Name="M02_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../fpga/Motor/M02_ALFA_RP_State.lvsc"/>
				<Item Name="M03_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../fpga/Motor/M03_ALFA_RP_State.lvsc"/>
				<Item Name="M04_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../fpga/Motor/M04_ALFA_RP_State.lvsc"/>
			</Item>
			<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
				<Property Name="FPGA.PersistentID" Type="Str">{5D7F7C8A-D26B-485C-AF42-ABC9E484BF6A}</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
			</Item>
			<Item Name="IP Builder" Type="IP Builder Target">
				<Item Name="Dependencies" Type="Dependencies"/>
				<Item Name="Build Specifications" Type="Build"/>
			</Item>
			<Item Name="Chassis1" Type="RIO Expansion Chassis">
				<Property Name="crio.Location" Type="Str">Connector 1</Property>
				<Item Name="Chassis1_Mod1" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Connector 1/Slot 1</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9425</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{107ACC86-B407-4149-9CC0-5513DCBEAEB8}</Property>
				</Item>
				<Item Name="Chassis1_Mod2" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Connector 1/Slot 2</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9477</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{AA7AAD24-9C05-4E75-A2E8-981B8C1E2C17}</Property>
				</Item>
				<Item Name="Chassis1_Mod3" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Connector 1/Slot 3</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9425</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{AF72DB20-FDCA-4063-A278-74A77EB04F3E}</Property>
				</Item>
			</Item>
			<Item Name="fpga_MotorControl.vi" Type="VI" URL="../fpga/Motor/fpga_MotorControl.vi">
				<Property Name="BuildSpec" Type="Str">{1BA8B31C-B5F0-495A-BC8A-4E37EA391323}</Property>
				<Property Name="configString.guid" Type="Str">{0556D37E-90B9-4E1C-9500-E9D99FC0470D}resource=/crio_Chassis1_Mod1/DI21;0;ReadMethodType=bool{05B167CA-74CA-4A1F-B6F3-1433B8C464B0}resource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool{06B03385-1198-4227-9EE1-1A68F6A66CF6}resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=bool{092B54D3-F319-44DF-B5C5-FB6C65250EF6}resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=bool{0A556816-8B06-4BD7-8E2B-BBA4AA9941F6}resource=/crio_Chassis1_Mod1/DI0;0;ReadMethodType=bool{0B7B93DC-F81A-4F27-BF0A-0173A6C65A56}resource=/crio_Chassis1_Mod1/DI18;0;ReadMethodType=bool{0C5A6A27-AFE2-47B3-BEF8-ABEF68CDF6AD}resource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool{0D6468E2-3FD5-4759-8248-2A0DCCDE7883}resource=/crio_Chassis1_Mod1/DI28;0;ReadMethodType=bool{107ACC86-B407-4149-9CC0-5513DCBEAEB8}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{11809FA0-432E-4CB3-8955-6FB1161A4171}resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=bool{11D0A347-B69B-46E4-ADC4-111D1CBB2726}resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=bool{1DD3F40A-58E6-4AC9-AA0D-382E0A4F0C96}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{24554BDC-6DAC-40EE-A7E4-A91DBF460EEC}resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool{25CB0153-BB77-4844-8B4D-D0C3612B4DD7}resource=/crio_Chassis1_Mod1/DI29;0;ReadMethodType=bool{289B4871-4F53-42C5-A2C7-5513ABDF69ED}resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=bool{3384035F-C4D0-4CA2-83C7-018907E1504E}resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=bool{35132869-D22F-4F18-9935-930332E0FC22}resource=/crio_Chassis1_Mod1/DI17;0;ReadMethodType=bool{356EE839-06ED-48FE-967D-1B7E727F9E6E}resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=bool{3570E1B0-405C-4FC1-BC50-507AEEC433E6}resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=bool{394F3452-21A5-4B7A-96FB-D7DDDA8BE291}resource=/crio_Chassis1_Mod1/DI23;0;ReadMethodType=bool{3C8CBFF7-B97D-4A2E-B47C-5F26D74576E3}resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=bool{3F65A59B-EE0A-4EB6-8D2F-C8901898DA81}resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=bool{43B9D847-69FF-4D49-B8E2-C185DBA2E33B}resource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=bool{4537DE5C-CD24-4B92-8154-7E72D5412F57}resource=/crio_Chassis1_Mod3/DI23;0;ReadMethodType=bool{499FFC76-8829-4838-BFAC-4213C896EE6E}resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=bool{49A6EEE5-22D4-4F71-8846-2A3F8DE54B0D}resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=bool{4B538F5A-A88F-40DC-B68F-05D9EAB976FF}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{5143860F-7E77-4B0E-B0FA-618983E8BF75}resource=/crio_Chassis1_Mod1/DI5;0;ReadMethodType=bool{54348C86-CD9E-4CDD-8086-0744670327EB}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{55C8C287-76AD-433C-AC8A-A4D4ECD9D82F}resource=/crio_Chassis1_Mod1/DI12;0;ReadMethodType=bool{55FE2007-EC58-4390-B09C-50E4CFDC84F6}resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=bool{595D7BDB-CE99-4B33-884D-A449DED7DF1F}resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=bool{5AFE7AC4-2AEB-48FF-9427-906EA6DCCDB8}resource=/crio_Chassis1_Mod1/DI11;0;ReadMethodType=bool{5D7F7C8A-D26B-485C-AF42-ABC9E484BF6A}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{5D9F73EB-38B9-41E4-A321-BC35A536B5F2}resource=/crio_Chassis1_Mod3/DI20;0;ReadMethodType=bool{61438BB2-0BEF-43B8-BB66-958D9E5E58B3}resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=bool{624B770E-F680-46DD-8B3E-7906D07D694E}resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=bool{63562E57-1DB7-487A-BBAE-BEC1EA804336}resource=/crio_Chassis1_Mod1/DI13;0;ReadMethodType=bool{6C418BAC-2150-46F7-8E7E-B62F91EF7EE2}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{790B0AF0-14E3-4353-B54F-E4D0514831B1}resource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=bool{79E8A5A9-D2D8-4616-9BDB-EAB5DA01BA73}resource=/crio_Chassis1_Mod1/DI2;0;ReadMethodType=bool{7A2528D8-D4F3-44ED-8D04-CF7636939773}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{7C303257-81DB-4EB4-B16D-A4C407320085}resource=/crio_Chassis1_Mod1/DI7;0;ReadMethodType=bool{7CE7CFCA-B9BA-45F3-BB44-6CC1D0DEE941}resource=/crio_Chassis1_Mod3/DI21;0;ReadMethodType=bool{7F9A7EB3-C747-48B8-B0D0-8259813C9C37}resource=/crio_Chassis1_Mod1/DI15;0;ReadMethodType=bool{84AEB81F-79DD-4B98-8923-9A31C8392FCE}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{84B346B4-F091-4B7A-B4EB-64A001D358F0}resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=bool{85E5E6F2-E926-4D6F-9125-6B879BB88DDE}resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=bool{86B8A4AF-9DF6-48E1-8583-F555902F2935}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{86D327ED-BA4F-4823-98AC-D30E08C4501B}resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=bool{8717D8EB-042E-4EC6-A1DE-21BFBBBF26B3}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{87FB5AE6-C649-45E5-82FF-E05CA0DA03F3}resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=bool{8809A15E-611D-4E98-BFCB-C66B7930EEFD}resource=/crio_Chassis1_Mod1/DI3;0;ReadMethodType=bool{883EB41D-3391-44E9-87CB-6A5836B5E327}resource=/crio_Chassis1_Mod1/DI14;0;ReadMethodType=bool{88BB7CCE-977D-457E-BC2C-2D6DCA928E88}resource=/crio_Chassis1_Mod1/DI26;0;ReadMethodType=bool{8976858B-11B3-44C9-A583-D568A0CF0B54}resource=/crio_Chassis1_Mod1/DI6;0;ReadMethodType=bool{8D762FB0-711D-40D6-B3D3-425DA7DAB7E5}resource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=bool{992F089D-7544-40C6-9C1A-BE030D2F152D}resource=/crio_Chassis1_Mod1/DI10;0;ReadMethodType=bool{9AAC76B7-21CB-4252-AAAA-98006C6B2BA6}resource=/crio_Chassis1_Mod3/DI22;0;ReadMethodType=bool{9F63FA0C-E844-4960-BD9C-5F4BFABD6505}resource=/crio_Chassis1_Mod1/DI30;0;ReadMethodType=bool{9FCB72D8-E1B6-4D17-8DEF-116D9AE3504D}resource=/crio_Chassis1_Mod1/DI24;0;ReadMethodType=bool{A2C4AF18-4DAC-48C3-B958-0DD9A2074C70}resource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=bool{A5825855-A5C7-4786-8A83-4DFE22D2804C}resource=/crio_Chassis1_Mod1/DI16;0;ReadMethodType=bool{A5EC982B-052F-4529-A461-958B89D01CDF}resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=bool{A604AFC3-6806-4388-BD36-F8325FAB8319}resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool{AA7AAD24-9C05-4E75-A2E8-981B8C1E2C17}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{AF72DB20-FDCA-4063-A278-74A77EB04F3E}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{B0B80628-F030-429D-AACC-9311D9CFAE8D}resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=bool{B250CE91-D2AD-411C-8048-717C84C843B6}resource=/crio_Chassis1_Mod1/DI22;0;ReadMethodType=bool{BBFBE534-752C-4B05-A58A-74FBA33176AC}resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=bool{BDE238D6-A11D-4DCA-8214-A0458FC9206B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{C224D373-096C-4B09-AC7E-0D3BD9F4B004}resource=/crio_Chassis1_Mod1/DI19;0;ReadMethodType=bool{C75EB28A-4E67-4AD7-83A0-DE2B3ACAAEDC}resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=bool{CB73F8B6-4B01-43F5-8A39-0636F7D0D425}resource=/crio_Chassis1_Mod1/DI27;0;ReadMethodType=bool{CBF7C3CB-8CDF-479C-8B56-50DE4364D918}resource=/crio_Chassis1_Mod1/DI25;0;ReadMethodType=bool{CDBB159E-172E-4042-B68F-FE23137470FF}resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool{CF744ECD-633C-4E9C-B975-16468E74E5AB}resource=/crio_Chassis1_Mod1/DI31;0;ReadMethodType=bool{D79F9C1E-1133-4E0D-B0AF-C5F95845EB93}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{D92C418B-6E9B-4996-92AA-5E8886F4AE74}resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=bool{D96E4E8B-67CF-48AB-ABE7-D7C631C60ED1}resource=/crio_Chassis1_Mod1/DI9;0;ReadMethodType=bool{D977F5E5-72D5-4664-B839-CEC8E13AE14E}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{DA63B211-9107-4251-A772-48367B9740E0}resource=/crio_Chassis1_Mod1/DI1;0;ReadMethodType=bool{E45A28CB-E645-47DC-A6E0-108A37CB0822}resource=/crio_Chassis1_Mod1/DI4;0;ReadMethodType=bool{E5C640DD-24F1-491E-AA65-289EBE3C8647}resource=/crio_Chassis1_Mod1/DI8;0;ReadMethodType=bool{EFEEFF89-A53D-4578-874F-57741729690B}resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=bool{F9105A72-F1D0-4E0E-9228-EE51EDD742CA}resource=/crio_Chassis1_Mod1/DI20;0;ReadMethodType=bool{F926388E-E270-4107-A427-4597EACE1607}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{FF7CAAE0-2B12-4DED-8FED-B66258DA1B54}resource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">24V_Ok_M01resource=/crio_Chassis1_Mod1/DI0;0;ReadMethodType=bool24V_Ok_M02resource=/crio_Chassis1_Mod1/DI1;0;ReadMethodType=bool24V_Ok_M03resource=/crio_Chassis1_Mod1/DI2;0;ReadMethodType=bool24V_Ok_M04resource=/crio_Chassis1_Mod1/DI3;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;All_RP_Out_1resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=boolAll_RP_Out_2resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=boolChassis1_Mod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Copy_HomeSw_M01resource=/crio_Chassis1_Mod3/DI20;0;ReadMethodType=boolCopy_HomeSw_M02resource=/crio_Chassis1_Mod3/DI21;0;ReadMethodType=boolCopy_HomeSw_M03resource=/crio_Chassis1_Mod3/DI22;0;ReadMethodType=boolCopy_HomeSw_M04resource=/crio_Chassis1_Mod3/DI23;0;ReadMethodType=boolDevice_Allowedresource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=boolDirection_M01resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=boolDirection_M02resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=boolDirection_M03resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=boolDirection_M04resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=boolDriverFlt_M01resource=/crio_Chassis1_Mod1/DI20;0;ReadMethodType=boolDriverFlt_M02resource=/crio_Chassis1_Mod1/DI21;0;ReadMethodType=boolDriverFlt_M03resource=/crio_Chassis1_Mod1/DI22;0;ReadMethodType=boolDriverFlt_M04resource=/crio_Chassis1_Mod1/DI23;0;ReadMethodType=boolEnable_M01resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=boolEnable_M02resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=boolEnable_M03resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=boolEnable_M04resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=boolImminent_Beam_Abortresource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=boolInjection_Inhibit_1resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=boolInjection_Inhibit_2resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=boolLVDT_Flt_Copy_M01resource=/crio_Chassis1_Mod1/DI25;0;ReadMethodType=boolLVDT_Flt_Copy_M02resource=/crio_Chassis1_Mod1/DI27;0;ReadMethodType=boolLVDT_Flt_Copy_M03resource=/crio_Chassis1_Mod1/DI29;0;ReadMethodType=boolLVDT_Flt_Copy_M04resource=/crio_Chassis1_Mod1/DI31;0;ReadMethodType=boolMotor_Driver_Offresource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=boolMvt_inhibitresource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=boolNot_Back_Homeresource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=boolOutOfLimits_Copy_M01resource=/crio_Chassis1_Mod1/DI24;0;ReadMethodType=boolOutOfLimits_Copy_M02resource=/crio_Chassis1_Mod1/DI26;0;ReadMethodType=boolOutOfLimits_Copy_M03resource=/crio_Chassis1_Mod1/DI28;0;ReadMethodType=boolOutOfLimits_Copy_M04resource=/crio_Chassis1_Mod1/DI30;0;ReadMethodType=boolOverride_1resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=boolOverride_2resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Reset_M01resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=boolReset_M02resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=boolReset_M03resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=boolReset_M04resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=boolResolver_Cos_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16Resolver_Cos_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16Resolver_Cos_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16Resolver_Cos_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Resolver_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16Resolver_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16Resolver_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16Resolver_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16Resolver_Sin_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16Resolver_Sin_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16Resolver_Sin_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16Resolver_Sin_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16Safe_Beam_1resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=boolSafe_Beam_2resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=boolSpare_1resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=boolSpare_2resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=boolStable_Beamresource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=boolStep_M01resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=boolStep_M02resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=boolStep_M03resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=boolStep_M04resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=boolStopper_In_M01resource=/crio_Chassis1_Mod1/DI7;0;ReadMethodType=boolStopper_In_M02resource=/crio_Chassis1_Mod1/DI11;0;ReadMethodType=boolStopper_In_M03resource=/crio_Chassis1_Mod1/DI15;0;ReadMethodType=boolStopper_In_M04resource=/crio_Chassis1_Mod1/DI19;0;ReadMethodType=boolStopper_Out_M01resource=/crio_Chassis1_Mod1/DI6;0;ReadMethodType=boolStopper_Out_M02resource=/crio_Chassis1_Mod1/DI10;0;ReadMethodType=boolStopper_Out_M03resource=/crio_Chassis1_Mod1/DI14;0;ReadMethodType=boolStopper_Out_M04resource=/crio_Chassis1_Mod1/DI18;0;ReadMethodType=boolSw_In_M01resource=/crio_Chassis1_Mod1/DI5;0;ReadMethodType=boolSw_In_M02resource=/crio_Chassis1_Mod1/DI9;0;ReadMethodType=boolSw_In_M03resource=/crio_Chassis1_Mod1/DI13;0;ReadMethodType=boolSw_In_M04resource=/crio_Chassis1_Mod1/DI17;0;ReadMethodType=boolSw_Out_M01resource=/crio_Chassis1_Mod1/DI4;0;ReadMethodType=boolSw_Out_M02resource=/crio_Chassis1_Mod1/DI8;0;ReadMethodType=boolSw_Out_M03resource=/crio_Chassis1_Mod1/DI12;0;ReadMethodType=boolSw_Out_M04resource=/crio_Chassis1_Mod1/DI16;0;ReadMethodType=boolUser_Permit_1aresource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=boolUser_Permit_1bresource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=boolUser_Permit_2aresource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=boolUser_Permit_2bresource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_AFP\FPGA Bitfiles\RPCSAFP_MotorControl_fpgaMotorControl_RcTzj95KYzA.lvbitx</Property>
			</Item>
			<Item Name="fpga_MotorControl_AFP_7852R_V2.vi" Type="VI" URL="../fpga/Motor/fpga_MotorControl_AFP_7852R_V2.vi">
				<Property Name="BuildSpec" Type="Str">{A014F0CC-335B-4E94-B01A-E44B24A9247E}</Property>
				<Property Name="configString.guid" Type="Str">{0556D37E-90B9-4E1C-9500-E9D99FC0470D}resource=/crio_Chassis1_Mod1/DI21;0;ReadMethodType=bool{05B167CA-74CA-4A1F-B6F3-1433B8C464B0}resource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool{06B03385-1198-4227-9EE1-1A68F6A66CF6}resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=bool{092B54D3-F319-44DF-B5C5-FB6C65250EF6}resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=bool{0A556816-8B06-4BD7-8E2B-BBA4AA9941F6}resource=/crio_Chassis1_Mod1/DI0;0;ReadMethodType=bool{0B7B93DC-F81A-4F27-BF0A-0173A6C65A56}resource=/crio_Chassis1_Mod1/DI18;0;ReadMethodType=bool{0C5A6A27-AFE2-47B3-BEF8-ABEF68CDF6AD}resource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool{0D6468E2-3FD5-4759-8248-2A0DCCDE7883}resource=/crio_Chassis1_Mod1/DI28;0;ReadMethodType=bool{107ACC86-B407-4149-9CC0-5513DCBEAEB8}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{11809FA0-432E-4CB3-8955-6FB1161A4171}resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=bool{11D0A347-B69B-46E4-ADC4-111D1CBB2726}resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=bool{1DD3F40A-58E6-4AC9-AA0D-382E0A4F0C96}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{24554BDC-6DAC-40EE-A7E4-A91DBF460EEC}resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool{25CB0153-BB77-4844-8B4D-D0C3612B4DD7}resource=/crio_Chassis1_Mod1/DI29;0;ReadMethodType=bool{289B4871-4F53-42C5-A2C7-5513ABDF69ED}resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=bool{3384035F-C4D0-4CA2-83C7-018907E1504E}resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=bool{35132869-D22F-4F18-9935-930332E0FC22}resource=/crio_Chassis1_Mod1/DI17;0;ReadMethodType=bool{356EE839-06ED-48FE-967D-1B7E727F9E6E}resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=bool{3570E1B0-405C-4FC1-BC50-507AEEC433E6}resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=bool{394F3452-21A5-4B7A-96FB-D7DDDA8BE291}resource=/crio_Chassis1_Mod1/DI23;0;ReadMethodType=bool{3C8CBFF7-B97D-4A2E-B47C-5F26D74576E3}resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=bool{3F65A59B-EE0A-4EB6-8D2F-C8901898DA81}resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=bool{43B9D847-69FF-4D49-B8E2-C185DBA2E33B}resource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=bool{4537DE5C-CD24-4B92-8154-7E72D5412F57}resource=/crio_Chassis1_Mod3/DI23;0;ReadMethodType=bool{499FFC76-8829-4838-BFAC-4213C896EE6E}resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=bool{49A6EEE5-22D4-4F71-8846-2A3F8DE54B0D}resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=bool{4B538F5A-A88F-40DC-B68F-05D9EAB976FF}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{5143860F-7E77-4B0E-B0FA-618983E8BF75}resource=/crio_Chassis1_Mod1/DI5;0;ReadMethodType=bool{54348C86-CD9E-4CDD-8086-0744670327EB}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{55C8C287-76AD-433C-AC8A-A4D4ECD9D82F}resource=/crio_Chassis1_Mod1/DI12;0;ReadMethodType=bool{55FE2007-EC58-4390-B09C-50E4CFDC84F6}resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=bool{595D7BDB-CE99-4B33-884D-A449DED7DF1F}resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=bool{5AFE7AC4-2AEB-48FF-9427-906EA6DCCDB8}resource=/crio_Chassis1_Mod1/DI11;0;ReadMethodType=bool{5D7F7C8A-D26B-485C-AF42-ABC9E484BF6A}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{5D9F73EB-38B9-41E4-A321-BC35A536B5F2}resource=/crio_Chassis1_Mod3/DI20;0;ReadMethodType=bool{61438BB2-0BEF-43B8-BB66-958D9E5E58B3}resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=bool{624B770E-F680-46DD-8B3E-7906D07D694E}resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=bool{63562E57-1DB7-487A-BBAE-BEC1EA804336}resource=/crio_Chassis1_Mod1/DI13;0;ReadMethodType=bool{6C418BAC-2150-46F7-8E7E-B62F91EF7EE2}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{790B0AF0-14E3-4353-B54F-E4D0514831B1}resource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=bool{79E8A5A9-D2D8-4616-9BDB-EAB5DA01BA73}resource=/crio_Chassis1_Mod1/DI2;0;ReadMethodType=bool{7A2528D8-D4F3-44ED-8D04-CF7636939773}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{7C303257-81DB-4EB4-B16D-A4C407320085}resource=/crio_Chassis1_Mod1/DI7;0;ReadMethodType=bool{7CE7CFCA-B9BA-45F3-BB44-6CC1D0DEE941}resource=/crio_Chassis1_Mod3/DI21;0;ReadMethodType=bool{7F9A7EB3-C747-48B8-B0D0-8259813C9C37}resource=/crio_Chassis1_Mod1/DI15;0;ReadMethodType=bool{84AEB81F-79DD-4B98-8923-9A31C8392FCE}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{84B346B4-F091-4B7A-B4EB-64A001D358F0}resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=bool{85E5E6F2-E926-4D6F-9125-6B879BB88DDE}resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=bool{86B8A4AF-9DF6-48E1-8583-F555902F2935}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{86D327ED-BA4F-4823-98AC-D30E08C4501B}resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=bool{8717D8EB-042E-4EC6-A1DE-21BFBBBF26B3}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{87FB5AE6-C649-45E5-82FF-E05CA0DA03F3}resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=bool{8809A15E-611D-4E98-BFCB-C66B7930EEFD}resource=/crio_Chassis1_Mod1/DI3;0;ReadMethodType=bool{883EB41D-3391-44E9-87CB-6A5836B5E327}resource=/crio_Chassis1_Mod1/DI14;0;ReadMethodType=bool{88BB7CCE-977D-457E-BC2C-2D6DCA928E88}resource=/crio_Chassis1_Mod1/DI26;0;ReadMethodType=bool{8976858B-11B3-44C9-A583-D568A0CF0B54}resource=/crio_Chassis1_Mod1/DI6;0;ReadMethodType=bool{8D762FB0-711D-40D6-B3D3-425DA7DAB7E5}resource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=bool{992F089D-7544-40C6-9C1A-BE030D2F152D}resource=/crio_Chassis1_Mod1/DI10;0;ReadMethodType=bool{9AAC76B7-21CB-4252-AAAA-98006C6B2BA6}resource=/crio_Chassis1_Mod3/DI22;0;ReadMethodType=bool{9F63FA0C-E844-4960-BD9C-5F4BFABD6505}resource=/crio_Chassis1_Mod1/DI30;0;ReadMethodType=bool{9FCB72D8-E1B6-4D17-8DEF-116D9AE3504D}resource=/crio_Chassis1_Mod1/DI24;0;ReadMethodType=bool{A2C4AF18-4DAC-48C3-B958-0DD9A2074C70}resource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=bool{A5825855-A5C7-4786-8A83-4DFE22D2804C}resource=/crio_Chassis1_Mod1/DI16;0;ReadMethodType=bool{A5EC982B-052F-4529-A461-958B89D01CDF}resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=bool{A604AFC3-6806-4388-BD36-F8325FAB8319}resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool{AA7AAD24-9C05-4E75-A2E8-981B8C1E2C17}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{AF72DB20-FDCA-4063-A278-74A77EB04F3E}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{B0B80628-F030-429D-AACC-9311D9CFAE8D}resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=bool{B250CE91-D2AD-411C-8048-717C84C843B6}resource=/crio_Chassis1_Mod1/DI22;0;ReadMethodType=bool{BBFBE534-752C-4B05-A58A-74FBA33176AC}resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=bool{BDE238D6-A11D-4DCA-8214-A0458FC9206B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{C224D373-096C-4B09-AC7E-0D3BD9F4B004}resource=/crio_Chassis1_Mod1/DI19;0;ReadMethodType=bool{C75EB28A-4E67-4AD7-83A0-DE2B3ACAAEDC}resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=bool{CB73F8B6-4B01-43F5-8A39-0636F7D0D425}resource=/crio_Chassis1_Mod1/DI27;0;ReadMethodType=bool{CBF7C3CB-8CDF-479C-8B56-50DE4364D918}resource=/crio_Chassis1_Mod1/DI25;0;ReadMethodType=bool{CDBB159E-172E-4042-B68F-FE23137470FF}resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool{CF744ECD-633C-4E9C-B975-16468E74E5AB}resource=/crio_Chassis1_Mod1/DI31;0;ReadMethodType=bool{D79F9C1E-1133-4E0D-B0AF-C5F95845EB93}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{D92C418B-6E9B-4996-92AA-5E8886F4AE74}resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=bool{D96E4E8B-67CF-48AB-ABE7-D7C631C60ED1}resource=/crio_Chassis1_Mod1/DI9;0;ReadMethodType=bool{D977F5E5-72D5-4664-B839-CEC8E13AE14E}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{DA63B211-9107-4251-A772-48367B9740E0}resource=/crio_Chassis1_Mod1/DI1;0;ReadMethodType=bool{E45A28CB-E645-47DC-A6E0-108A37CB0822}resource=/crio_Chassis1_Mod1/DI4;0;ReadMethodType=bool{E5C640DD-24F1-491E-AA65-289EBE3C8647}resource=/crio_Chassis1_Mod1/DI8;0;ReadMethodType=bool{EFEEFF89-A53D-4578-874F-57741729690B}resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=bool{F9105A72-F1D0-4E0E-9228-EE51EDD742CA}resource=/crio_Chassis1_Mod1/DI20;0;ReadMethodType=bool{F926388E-E270-4107-A427-4597EACE1607}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{FF7CAAE0-2B12-4DED-8FED-B66258DA1B54}resource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">24V_Ok_M01resource=/crio_Chassis1_Mod1/DI0;0;ReadMethodType=bool24V_Ok_M02resource=/crio_Chassis1_Mod1/DI1;0;ReadMethodType=bool24V_Ok_M03resource=/crio_Chassis1_Mod1/DI2;0;ReadMethodType=bool24V_Ok_M04resource=/crio_Chassis1_Mod1/DI3;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;All_RP_Out_1resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=boolAll_RP_Out_2resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=boolChassis1_Mod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Copy_HomeSw_M01resource=/crio_Chassis1_Mod3/DI20;0;ReadMethodType=boolCopy_HomeSw_M02resource=/crio_Chassis1_Mod3/DI21;0;ReadMethodType=boolCopy_HomeSw_M03resource=/crio_Chassis1_Mod3/DI22;0;ReadMethodType=boolCopy_HomeSw_M04resource=/crio_Chassis1_Mod3/DI23;0;ReadMethodType=boolDevice_Allowedresource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=boolDirection_M01resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=boolDirection_M02resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=boolDirection_M03resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=boolDirection_M04resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=boolDriverFlt_M01resource=/crio_Chassis1_Mod1/DI20;0;ReadMethodType=boolDriverFlt_M02resource=/crio_Chassis1_Mod1/DI21;0;ReadMethodType=boolDriverFlt_M03resource=/crio_Chassis1_Mod1/DI22;0;ReadMethodType=boolDriverFlt_M04resource=/crio_Chassis1_Mod1/DI23;0;ReadMethodType=boolEnable_M01resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=boolEnable_M02resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=boolEnable_M03resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=boolEnable_M04resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=boolImminent_Beam_Abortresource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=boolInjection_Inhibit_1resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=boolInjection_Inhibit_2resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=boolLVDT_Flt_Copy_M01resource=/crio_Chassis1_Mod1/DI25;0;ReadMethodType=boolLVDT_Flt_Copy_M02resource=/crio_Chassis1_Mod1/DI27;0;ReadMethodType=boolLVDT_Flt_Copy_M03resource=/crio_Chassis1_Mod1/DI29;0;ReadMethodType=boolLVDT_Flt_Copy_M04resource=/crio_Chassis1_Mod1/DI31;0;ReadMethodType=boolMotor_Driver_Offresource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=boolMvt_inhibitresource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=boolNot_Back_Homeresource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=boolOutOfLimits_Copy_M01resource=/crio_Chassis1_Mod1/DI24;0;ReadMethodType=boolOutOfLimits_Copy_M02resource=/crio_Chassis1_Mod1/DI26;0;ReadMethodType=boolOutOfLimits_Copy_M03resource=/crio_Chassis1_Mod1/DI28;0;ReadMethodType=boolOutOfLimits_Copy_M04resource=/crio_Chassis1_Mod1/DI30;0;ReadMethodType=boolOverride_1resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=boolOverride_2resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Reset_M01resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=boolReset_M02resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=boolReset_M03resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=boolReset_M04resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=boolResolver_Cos_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16Resolver_Cos_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16Resolver_Cos_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16Resolver_Cos_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Resolver_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16Resolver_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16Resolver_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16Resolver_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16Resolver_Sin_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16Resolver_Sin_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16Resolver_Sin_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16Resolver_Sin_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16Safe_Beam_1resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=boolSafe_Beam_2resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=boolSpare_1resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=boolSpare_2resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=boolStable_Beamresource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=boolStep_M01resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=boolStep_M02resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=boolStep_M03resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=boolStep_M04resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=boolStopper_In_M01resource=/crio_Chassis1_Mod1/DI7;0;ReadMethodType=boolStopper_In_M02resource=/crio_Chassis1_Mod1/DI11;0;ReadMethodType=boolStopper_In_M03resource=/crio_Chassis1_Mod1/DI15;0;ReadMethodType=boolStopper_In_M04resource=/crio_Chassis1_Mod1/DI19;0;ReadMethodType=boolStopper_Out_M01resource=/crio_Chassis1_Mod1/DI6;0;ReadMethodType=boolStopper_Out_M02resource=/crio_Chassis1_Mod1/DI10;0;ReadMethodType=boolStopper_Out_M03resource=/crio_Chassis1_Mod1/DI14;0;ReadMethodType=boolStopper_Out_M04resource=/crio_Chassis1_Mod1/DI18;0;ReadMethodType=boolSw_In_M01resource=/crio_Chassis1_Mod1/DI5;0;ReadMethodType=boolSw_In_M02resource=/crio_Chassis1_Mod1/DI9;0;ReadMethodType=boolSw_In_M03resource=/crio_Chassis1_Mod1/DI13;0;ReadMethodType=boolSw_In_M04resource=/crio_Chassis1_Mod1/DI17;0;ReadMethodType=boolSw_Out_M01resource=/crio_Chassis1_Mod1/DI4;0;ReadMethodType=boolSw_Out_M02resource=/crio_Chassis1_Mod1/DI8;0;ReadMethodType=boolSw_Out_M03resource=/crio_Chassis1_Mod1/DI12;0;ReadMethodType=boolSw_Out_M04resource=/crio_Chassis1_Mod1/DI16;0;ReadMethodType=boolUser_Permit_1aresource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=boolUser_Permit_1bresource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=boolUser_Permit_2aresource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=boolUser_Permit_2bresource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_AFP\FPGA Bitfiles\RPCSAFP_MotorControl_fpgaMotorControl_ZgD4Qsbs2ys.lvbitx</Property>
			</Item>
			<Item Name="Dependencies" Type="Dependencies">
				<Item Name="vi.lib" Type="Folder">
					<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
					<Item Name="NI_SC_LVSCCommonFiles.lvlib" Type="Library" URL="/&lt;vilib&gt;/Statechart/Common/NI_SC_LVSCCommonFiles.lvlib"/>
					<Item Name="SCRT Dbg Rtn.vi" Type="VI" URL="/&lt;vilib&gt;/Statechart/Infrastructure/RTStatechart/Dbg/SCRT Dbg Rtn.vi"/>
					<Item Name="SCRT SDV Rtn.vi" Type="VI" URL="/&lt;vilib&gt;/Statechart/Infrastructure/RTStatechart/Dbg/SCRT SDV Rtn.vi"/>
				</Item>
				<Item Name="instr.lib" Type="Folder">
					<Item Name="niInstr Basic Elements v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/niInstr Basic Elements v1 FPGA.lvlib"/>
				</Item>
			</Item>
			<Item Name="Build Specifications" Type="Build">
				<Item Name="fpga_MotorControl" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_MotorControl</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPCSAFP_MotorControl_fpgaMotorControl_RcTzj95KYzA.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_AFP/FPGA Bitfiles/RPCSAFP_MotorControl_fpgaMotorControl_RcTzj95KYzA.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPCSAFP_MotorControl_fpgaMotorControl_RcTzj95KYzA.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_AFP/RPCS_AFP.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
					<Property Name="TargetName" Type="Str">MotorControl</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/MotorControl/fpga_MotorControl.vi</Property>
				</Item>
				<Item Name="fpga_MotorControl_AFP_7852R_V2" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_MotorControl_AFP_7852R_V2</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPCSAFP_MotorControl_fpgaMotorControl_ZgD4Qsbs2ys.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_AFP/FPGA Bitfiles/RPCSAFP_MotorControl_fpgaMotorControl_ZgD4Qsbs2ys.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPCSAFP_MotorControl_fpgaMotorControl_ZgD4Qsbs2ys.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_AFP/RPCS_AFP.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
					<Property Name="TargetName" Type="Str">MotorControl</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/MotorControl/fpga_MotorControl_AFP_7852R_V2.vi</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="LVDTCalc" Type="FPGA Target">
			<Property Name="AutoRun" Type="Bool">false</Property>
			<Property Name="configString.guid" Type="Str">{08CDFA3A-7C35-40D1-9DA2-40EFCCD47752}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{0A582429-0BFE-40E4-9712-37AF557F0BBB}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{1706EE68-BF3A-45F3-9933-BA2D257D4475}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{20542714-CE83-4D7E-9A4D-DF4354004719}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH3;0;ReadMethodType=bool;WriteMethodType=bool{22776981-36DB-40EF-8098-B787506C1DF6}resource=/crio_Chassis1_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=bool{2D65D439-4A25-47DE-BA19-0F1FCA327745}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{426078CC-340B-4FF7-A73D-57B7F8E8F71F}resource=/crio_Chassis1_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=bool{5F95C7FF-ABD4-43B2-BCB7-0083A4E9C3DD}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{69310FCA-4C28-4C90-A35E-BF723B8EA04B}resource=/crio_Chassis1_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=bool{695ACB1B-E4F1-4AF1-BCFE-DE97F757C6EE}resource=/crio_Chassis1_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=bool{6A817F8E-4E0C-4648-9DC3-D90D22CC3E34}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{70E85B13-D7B9-4CDF-B8FF-E939C6F4AA02}resource=/crio_Chassis1_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=bool{732EE144-32E3-45DB-B440-E01FBDACC102}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{7A1E51F5-D1D7-48D9-A99F-E5428CB746DF}resource=/crio_Chassis1_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=bool{9C0CD391-E157-4C01-AC2F-C8F92590AAC7}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9481,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{A5F7B6CE-8F02-4DA7-8CB0-91F64C97F905}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{B343A9D4-324D-4C7D-8BF7-93A8FC66D7D8}resource=/crio_Chassis1_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=bool{B9114A29-59DC-45A4-9EC4-8DB767CBCDAD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{BA6E6398-0533-4EA1-A6E8-82AE47999CF5}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{BE6563F0-A700-4608-9E01-45B5ECC859AF}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH0;0;ReadMethodType=bool;WriteMethodType=bool{C1C88408-17A2-4F2E-877A-C7D18BB6A02B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{CCF00DA0-1C48-4447-8D0F-5333142C9269}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{DAE836CD-371E-45F4-9C64-62626BE783D4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{DC490A36-E08A-44BA-B2CD-2A6295489E79}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{DE4E5E8F-7997-4C88-84AF-1502BB865925}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{DF05FEAA-C8A5-498F-99BA-B69CC19FFC87}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{E2DD37A9-4BB1-4387-81B9-E61C31488648}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{E5C92DBC-132E-4234-9043-7BC579F28B5E}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH2;0;ReadMethodType=bool;WriteMethodType=bool{E78A182D-B879-4D95-8163-162036538AF9}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH1;0;ReadMethodType=bool;WriteMethodType=bool{F0C44365-0884-4C82-9C81-9E1010BDB396}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{FF2EBF26-B693-402E-9479-5A4F5A40A11F}resource=/crio_Chassis1_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis1_Mod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9481,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]Interlock_M01ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH0;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M02ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH1;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M03ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH2;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M04ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH3;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16LVDT_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16LVDT_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16LVDT_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16LVDT_Flt_M01resource=/crio_Chassis1_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M02resource=/crio_Chassis1_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M03resource=/crio_Chassis1_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M04resource=/crio_Chassis1_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=boolLVDT_M01_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M02_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M03_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M04_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_V1_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16LVDT_V1_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16LVDT_V1_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16LVDT_V1_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16LVDT_V2_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16LVDT_V2_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16LVDT_V2_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16LVDT_V2_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16OutOfLimits_M01resource=/crio_Chassis1_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M02resource=/crio_Chassis1_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M03resource=/crio_Chassis1_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M04resource=/crio_Chassis1_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">PXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA</Property>
			<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
			<Property Name="Resource Name" Type="Str">RIO1</Property>
			<Property Name="Target Class" Type="Str">PXI-7852R</Property>
			<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
			<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
			<Item Name="Exchange with Motor Ctrl PXI" Type="Folder">
				<Item Name="OutOfLimits_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FF2EBF26-B693-402E-9479-5A4F5A40A11F}</Property>
				</Item>
				<Item Name="LVDT_Flt_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{70E85B13-D7B9-4CDF-B8FF-E939C6F4AA02}</Property>
				</Item>
				<Item Name="OutOfLimits_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7A1E51F5-D1D7-48D9-A99F-E5428CB746DF}</Property>
				</Item>
				<Item Name="LVDT_Flt_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{695ACB1B-E4F1-4AF1-BCFE-DE97F757C6EE}</Property>
				</Item>
				<Item Name="OutOfLimits_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DO4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B343A9D4-324D-4C7D-8BF7-93A8FC66D7D8}</Property>
				</Item>
				<Item Name="LVDT_Flt_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DO5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{69310FCA-4C28-4C90-A35E-BF723B8EA04B}</Property>
				</Item>
				<Item Name="OutOfLimits_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DO6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{22776981-36DB-40EF-8098-B787506C1DF6}</Property>
				</Item>
				<Item Name="LVDT_Flt_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod1/DO7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{426078CC-340B-4FF7-A73D-57B7F8E8F71F}</Property>
				</Item>
			</Item>
			<Item Name="Interlock Channels" Type="Folder">
				<Item Name="Interlock_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/CH0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BE6563F0-A700-4608-9E01-45B5ECC859AF}</Property>
				</Item>
				<Item Name="Interlock_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/CH1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E78A182D-B879-4D95-8163-162036538AF9}</Property>
				</Item>
				<Item Name="Interlock_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/CH2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E5C92DBC-132E-4234-9043-7BC579F28B5E}</Property>
				</Item>
				<Item Name="Interlock_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/CH3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{20542714-CE83-4D7E-9A4D-DF4354004719}</Property>
				</Item>
			</Item>
			<Item Name="LVDT Channels" Type="Folder">
				<Item Name="LVDT_V1_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A5F7B6CE-8F02-4DA7-8CB0-91F64C97F905}</Property>
				</Item>
				<Item Name="LVDT_V2_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1706EE68-BF3A-45F3-9933-BA2D257D4475}</Property>
				</Item>
				<Item Name="LVDT_V1_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DAE836CD-371E-45F4-9C64-62626BE783D4}</Property>
				</Item>
				<Item Name="LVDT_V2_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DE4E5E8F-7997-4C88-84AF-1502BB865925}</Property>
				</Item>
				<Item Name="LVDT_V1_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CCF00DA0-1C48-4447-8D0F-5333142C9269}</Property>
				</Item>
				<Item Name="LVDT_V2_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BA6E6398-0533-4EA1-A6E8-82AE47999CF5}</Property>
				</Item>
				<Item Name="LVDT_V1_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E2DD37A9-4BB1-4387-81B9-E61C31488648}</Property>
				</Item>
				<Item Name="LVDT_V2_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DC490A36-E08A-44BA-B2CD-2A6295489E79}</Property>
				</Item>
				<Item Name="LVDT_Exc_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F0C44365-0884-4C82-9C81-9E1010BDB396}</Property>
				</Item>
				<Item Name="LVDT_Exc_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DF05FEAA-C8A5-498F-99BA-B69CC19FFC87}</Property>
				</Item>
				<Item Name="LVDT_Exc_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C1C88408-17A2-4F2E-877A-C7D18BB6A02B}</Property>
				</Item>
				<Item Name="LVDT_Exc_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{08CDFA3A-7C35-40D1-9DA2-40EFCCD47752}</Property>
				</Item>
			</Item>
			<Item Name="FIFO Config" Type="Folder">
				<Item Name="LVDT_FIFO_element.ctl" Type="VI" URL="../fpga/LVDT/LVDT_FIFO_element.ctl">
					<Property Name="configString.guid" Type="Str">{08CDFA3A-7C35-40D1-9DA2-40EFCCD47752}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{0A582429-0BFE-40E4-9712-37AF557F0BBB}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{1706EE68-BF3A-45F3-9933-BA2D257D4475}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{20542714-CE83-4D7E-9A4D-DF4354004719}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH3;0;ReadMethodType=bool;WriteMethodType=bool{22776981-36DB-40EF-8098-B787506C1DF6}resource=/crio_Chassis1_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=bool{2D65D439-4A25-47DE-BA19-0F1FCA327745}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{426078CC-340B-4FF7-A73D-57B7F8E8F71F}resource=/crio_Chassis1_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=bool{5F95C7FF-ABD4-43B2-BCB7-0083A4E9C3DD}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{69310FCA-4C28-4C90-A35E-BF723B8EA04B}resource=/crio_Chassis1_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=bool{695ACB1B-E4F1-4AF1-BCFE-DE97F757C6EE}resource=/crio_Chassis1_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=bool{6A817F8E-4E0C-4648-9DC3-D90D22CC3E34}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{70E85B13-D7B9-4CDF-B8FF-E939C6F4AA02}resource=/crio_Chassis1_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=bool{732EE144-32E3-45DB-B440-E01FBDACC102}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{7A1E51F5-D1D7-48D9-A99F-E5428CB746DF}resource=/crio_Chassis1_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=bool{9C0CD391-E157-4C01-AC2F-C8F92590AAC7}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9481,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{A5F7B6CE-8F02-4DA7-8CB0-91F64C97F905}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{B343A9D4-324D-4C7D-8BF7-93A8FC66D7D8}resource=/crio_Chassis1_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=bool{B9114A29-59DC-45A4-9EC4-8DB767CBCDAD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{BA6E6398-0533-4EA1-A6E8-82AE47999CF5}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{BE6563F0-A700-4608-9E01-45B5ECC859AF}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH0;0;ReadMethodType=bool;WriteMethodType=bool{C1C88408-17A2-4F2E-877A-C7D18BB6A02B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{CCF00DA0-1C48-4447-8D0F-5333142C9269}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{DAE836CD-371E-45F4-9C64-62626BE783D4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{DC490A36-E08A-44BA-B2CD-2A6295489E79}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{DE4E5E8F-7997-4C88-84AF-1502BB865925}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{DF05FEAA-C8A5-498F-99BA-B69CC19FFC87}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{E2DD37A9-4BB1-4387-81B9-E61C31488648}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{E5C92DBC-132E-4234-9043-7BC579F28B5E}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH2;0;ReadMethodType=bool;WriteMethodType=bool{E78A182D-B879-4D95-8163-162036538AF9}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH1;0;ReadMethodType=bool;WriteMethodType=bool{F0C44365-0884-4C82-9C81-9E1010BDB396}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{FF2EBF26-B693-402E-9479-5A4F5A40A11F}resource=/crio_Chassis1_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
					<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis1_Mod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9481,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]Interlock_M01ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH0;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M02ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH1;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M03ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH2;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M04ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH3;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16LVDT_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16LVDT_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16LVDT_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16LVDT_Flt_M01resource=/crio_Chassis1_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M02resource=/crio_Chassis1_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M03resource=/crio_Chassis1_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M04resource=/crio_Chassis1_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=boolLVDT_M01_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M02_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M03_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M04_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_V1_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16LVDT_V1_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16LVDT_V1_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16LVDT_V1_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16LVDT_V2_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16LVDT_V2_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16LVDT_V2_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16LVDT_V2_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16OutOfLimits_M01resource=/crio_Chassis1_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M02resource=/crio_Chassis1_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M03resource=/crio_Chassis1_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M04resource=/crio_Chassis1_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				</Item>
				<Item Name="LVDT_M01_Acq" Type="FPGA FIFO">
					<Property Name="Actual Number of Elements" Type="UInt">100</Property>
					<Property Name="Arbitration for Read" Type="UInt">1</Property>
					<Property Name="Arbitration for Write" Type="UInt">1</Property>
					<Property Name="Control Logic" Type="UInt">0</Property>
					<Property Name="Data Type" Type="UInt">10</Property>
					<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
					<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
					<Property Name="fifo.configured" Type="Bool">true</Property>
					<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
					<Property Name="fifo.valid" Type="Bool">true</Property>
					<Property Name="fifo.version" Type="Int">12</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{732EE144-32E3-45DB-B440-E01FBDACC102}</Property>
					<Property Name="Local" Type="Bool">false</Property>
					<Property Name="Memory Type" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
					<Property Name="Requested Number of Elements" Type="UInt">100</Property>
					<Property Name="Type" Type="UInt">0</Property>
					<Property Name="Type Descriptor" Type="Str">100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000</Property>
				</Item>
				<Item Name="LVDT_M02_Acq" Type="FPGA FIFO">
					<Property Name="Actual Number of Elements" Type="UInt">100</Property>
					<Property Name="Arbitration for Read" Type="UInt">1</Property>
					<Property Name="Arbitration for Write" Type="UInt">1</Property>
					<Property Name="Control Logic" Type="UInt">0</Property>
					<Property Name="Data Type" Type="UInt">10</Property>
					<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
					<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
					<Property Name="fifo.configured" Type="Bool">true</Property>
					<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
					<Property Name="fifo.valid" Type="Bool">true</Property>
					<Property Name="fifo.version" Type="Int">12</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0A582429-0BFE-40E4-9712-37AF557F0BBB}</Property>
					<Property Name="Local" Type="Bool">false</Property>
					<Property Name="Memory Type" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
					<Property Name="Requested Number of Elements" Type="UInt">100</Property>
					<Property Name="Type" Type="UInt">0</Property>
					<Property Name="Type Descriptor" Type="Str">100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000</Property>
				</Item>
				<Item Name="LVDT_M03_Acq" Type="FPGA FIFO">
					<Property Name="Actual Number of Elements" Type="UInt">100</Property>
					<Property Name="Arbitration for Read" Type="UInt">1</Property>
					<Property Name="Arbitration for Write" Type="UInt">1</Property>
					<Property Name="Control Logic" Type="UInt">0</Property>
					<Property Name="Data Type" Type="UInt">10</Property>
					<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
					<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
					<Property Name="fifo.configured" Type="Bool">true</Property>
					<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
					<Property Name="fifo.valid" Type="Bool">true</Property>
					<Property Name="fifo.version" Type="Int">12</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6A817F8E-4E0C-4648-9DC3-D90D22CC3E34}</Property>
					<Property Name="Local" Type="Bool">false</Property>
					<Property Name="Memory Type" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
					<Property Name="Requested Number of Elements" Type="UInt">100</Property>
					<Property Name="Type" Type="UInt">0</Property>
					<Property Name="Type Descriptor" Type="Str">100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000</Property>
				</Item>
				<Item Name="LVDT_M04_Acq" Type="FPGA FIFO">
					<Property Name="Actual Number of Elements" Type="UInt">100</Property>
					<Property Name="Arbitration for Read" Type="UInt">1</Property>
					<Property Name="Arbitration for Write" Type="UInt">1</Property>
					<Property Name="Control Logic" Type="UInt">0</Property>
					<Property Name="Data Type" Type="UInt">10</Property>
					<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
					<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
					<Property Name="fifo.configured" Type="Bool">true</Property>
					<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
					<Property Name="fifo.valid" Type="Bool">true</Property>
					<Property Name="fifo.version" Type="Int">12</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5F95C7FF-ABD4-43B2-BCB7-0083A4E9C3DD}</Property>
					<Property Name="Local" Type="Bool">false</Property>
					<Property Name="Memory Type" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
					<Property Name="Requested Number of Elements" Type="UInt">100</Property>
					<Property Name="Type" Type="UInt">0</Property>
					<Property Name="Type Descriptor" Type="Str">100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000</Property>
				</Item>
			</Item>
			<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
				<Property Name="FPGA.PersistentID" Type="Str">{2D65D439-4A25-47DE-BA19-0F1FCA327745}</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
			</Item>
			<Item Name="IP Builder" Type="IP Builder Target">
				<Item Name="Dependencies" Type="Dependencies"/>
				<Item Name="Build Specifications" Type="Build"/>
			</Item>
			<Item Name="Chassis1" Type="RIO Expansion Chassis">
				<Property Name="crio.Location" Type="Str">Connector 1</Property>
				<Item Name="Chassis1_Mod1" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Connector 1/Slot 1</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9477</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B9114A29-59DC-45A4-9EC4-8DB767CBCDAD}</Property>
				</Item>
				<Item Name="Chassis1_Mod2" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Connector 1/Slot 2</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9481</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.DIO3_0InitialDir" Type="Str">0</Property>
					<Property Name="cRIOModule.DIO7_4InitialDir" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.NumSyncRegs" Type="Str">11111111</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9C0CD391-E157-4C01-AC2F-C8F92590AAC7}</Property>
				</Item>
			</Item>
			<Item Name="fpga_LVDT_v2.vi" Type="VI" URL="../fpga/LVDT/fpga_LVDT_v2.vi">
				<Property Name="BuildSpec" Type="Str">{5FD1647B-FC57-4402-914C-B61B9EBDFAC1}</Property>
				<Property Name="configString.guid" Type="Str">{08CDFA3A-7C35-40D1-9DA2-40EFCCD47752}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{0A582429-0BFE-40E4-9712-37AF557F0BBB}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{1706EE68-BF3A-45F3-9933-BA2D257D4475}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{20542714-CE83-4D7E-9A4D-DF4354004719}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH3;0;ReadMethodType=bool;WriteMethodType=bool{22776981-36DB-40EF-8098-B787506C1DF6}resource=/crio_Chassis1_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=bool{2D65D439-4A25-47DE-BA19-0F1FCA327745}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{426078CC-340B-4FF7-A73D-57B7F8E8F71F}resource=/crio_Chassis1_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=bool{5F95C7FF-ABD4-43B2-BCB7-0083A4E9C3DD}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{69310FCA-4C28-4C90-A35E-BF723B8EA04B}resource=/crio_Chassis1_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=bool{695ACB1B-E4F1-4AF1-BCFE-DE97F757C6EE}resource=/crio_Chassis1_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=bool{6A817F8E-4E0C-4648-9DC3-D90D22CC3E34}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{70E85B13-D7B9-4CDF-B8FF-E939C6F4AA02}resource=/crio_Chassis1_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=bool{732EE144-32E3-45DB-B440-E01FBDACC102}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{7A1E51F5-D1D7-48D9-A99F-E5428CB746DF}resource=/crio_Chassis1_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=bool{9C0CD391-E157-4C01-AC2F-C8F92590AAC7}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9481,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{A5F7B6CE-8F02-4DA7-8CB0-91F64C97F905}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{B343A9D4-324D-4C7D-8BF7-93A8FC66D7D8}resource=/crio_Chassis1_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=bool{B9114A29-59DC-45A4-9EC4-8DB767CBCDAD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{BA6E6398-0533-4EA1-A6E8-82AE47999CF5}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{BE6563F0-A700-4608-9E01-45B5ECC859AF}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH0;0;ReadMethodType=bool;WriteMethodType=bool{C1C88408-17A2-4F2E-877A-C7D18BB6A02B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{CCF00DA0-1C48-4447-8D0F-5333142C9269}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{DAE836CD-371E-45F4-9C64-62626BE783D4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{DC490A36-E08A-44BA-B2CD-2A6295489E79}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{DE4E5E8F-7997-4C88-84AF-1502BB865925}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{DF05FEAA-C8A5-498F-99BA-B69CC19FFC87}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{E2DD37A9-4BB1-4387-81B9-E61C31488648}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{E5C92DBC-132E-4234-9043-7BC579F28B5E}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH2;0;ReadMethodType=bool;WriteMethodType=bool{E78A182D-B879-4D95-8163-162036538AF9}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH1;0;ReadMethodType=bool;WriteMethodType=bool{F0C44365-0884-4C82-9C81-9E1010BDB396}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{FF2EBF26-B693-402E-9479-5A4F5A40A11F}resource=/crio_Chassis1_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis1_Mod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9481,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]Interlock_M01ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH0;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M02ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH1;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M03ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH2;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M04ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH3;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16LVDT_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16LVDT_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16LVDT_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16LVDT_Flt_M01resource=/crio_Chassis1_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M02resource=/crio_Chassis1_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M03resource=/crio_Chassis1_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M04resource=/crio_Chassis1_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=boolLVDT_M01_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M02_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M03_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M04_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_V1_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16LVDT_V1_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16LVDT_V1_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16LVDT_V1_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16LVDT_V2_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16LVDT_V2_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16LVDT_V2_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16LVDT_V2_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16OutOfLimits_M01resource=/crio_Chassis1_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M02resource=/crio_Chassis1_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M03resource=/crio_Chassis1_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M04resource=/crio_Chassis1_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_AFP\FPGA Bitfiles\RPCSAFP_LVDTCalc_fpgaLVDTv2_eC+9Bi5K1VA.lvbitx</Property>
			</Item>
			<Item Name="fpga_LVDT_Totem_4_pots.vi" Type="VI" URL="../fpga/LVDT/fpga_LVDT_Totem_4_pots.vi">
				<Property Name="BuildSpec" Type="Str">{2A24497F-8721-4599-9021-7569FDB04052}</Property>
				<Property Name="configString.guid" Type="Str">{08CDFA3A-7C35-40D1-9DA2-40EFCCD47752}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{0A582429-0BFE-40E4-9712-37AF557F0BBB}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{1706EE68-BF3A-45F3-9933-BA2D257D4475}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{20542714-CE83-4D7E-9A4D-DF4354004719}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH3;0;ReadMethodType=bool;WriteMethodType=bool{22776981-36DB-40EF-8098-B787506C1DF6}resource=/crio_Chassis1_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=bool{2D65D439-4A25-47DE-BA19-0F1FCA327745}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{426078CC-340B-4FF7-A73D-57B7F8E8F71F}resource=/crio_Chassis1_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=bool{5F95C7FF-ABD4-43B2-BCB7-0083A4E9C3DD}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{69310FCA-4C28-4C90-A35E-BF723B8EA04B}resource=/crio_Chassis1_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=bool{695ACB1B-E4F1-4AF1-BCFE-DE97F757C6EE}resource=/crio_Chassis1_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=bool{6A817F8E-4E0C-4648-9DC3-D90D22CC3E34}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{70E85B13-D7B9-4CDF-B8FF-E939C6F4AA02}resource=/crio_Chassis1_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=bool{732EE144-32E3-45DB-B440-E01FBDACC102}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{7A1E51F5-D1D7-48D9-A99F-E5428CB746DF}resource=/crio_Chassis1_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=bool{9C0CD391-E157-4C01-AC2F-C8F92590AAC7}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9481,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{A5F7B6CE-8F02-4DA7-8CB0-91F64C97F905}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{B343A9D4-324D-4C7D-8BF7-93A8FC66D7D8}resource=/crio_Chassis1_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=bool{B9114A29-59DC-45A4-9EC4-8DB767CBCDAD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{BA6E6398-0533-4EA1-A6E8-82AE47999CF5}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{BE6563F0-A700-4608-9E01-45B5ECC859AF}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH0;0;ReadMethodType=bool;WriteMethodType=bool{C1C88408-17A2-4F2E-877A-C7D18BB6A02B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{CCF00DA0-1C48-4447-8D0F-5333142C9269}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{DAE836CD-371E-45F4-9C64-62626BE783D4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{DC490A36-E08A-44BA-B2CD-2A6295489E79}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{DE4E5E8F-7997-4C88-84AF-1502BB865925}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{DF05FEAA-C8A5-498F-99BA-B69CC19FFC87}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{E2DD37A9-4BB1-4387-81B9-E61C31488648}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{E5C92DBC-132E-4234-9043-7BC579F28B5E}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH2;0;ReadMethodType=bool;WriteMethodType=bool{E78A182D-B879-4D95-8163-162036538AF9}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH1;0;ReadMethodType=bool;WriteMethodType=bool{F0C44365-0884-4C82-9C81-9E1010BDB396}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{FF2EBF26-B693-402E-9479-5A4F5A40A11F}resource=/crio_Chassis1_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis1_Mod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9481,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]Interlock_M01ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH0;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M02ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH1;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M03ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH2;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M04ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH3;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16LVDT_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16LVDT_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16LVDT_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16LVDT_Flt_M01resource=/crio_Chassis1_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M02resource=/crio_Chassis1_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M03resource=/crio_Chassis1_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M04resource=/crio_Chassis1_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=boolLVDT_M01_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M02_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M03_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M04_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_V1_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16LVDT_V1_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16LVDT_V1_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16LVDT_V1_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16LVDT_V2_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16LVDT_V2_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16LVDT_V2_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16LVDT_V2_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16OutOfLimits_M01resource=/crio_Chassis1_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M02resource=/crio_Chassis1_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M03resource=/crio_Chassis1_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M04resource=/crio_Chassis1_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_AFP\FPGA Bitfiles\RPCSAFP_LVDTCalc_fpgaLVDTTotem4po_pbHApeb4qOM.lvbitx</Property>
			</Item>
			<Item Name="fpga_LVDT_Totem_4_pots_v2.vi" Type="VI" URL="../fpga/LVDT/fpga_LVDT_Totem_4_pots_v2.vi">
				<Property Name="BuildSpec" Type="Str">{2D03CC4A-08BC-47A8-8120-9ADD2048FC72}</Property>
				<Property Name="configString.guid" Type="Str">{08CDFA3A-7C35-40D1-9DA2-40EFCCD47752}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{0A582429-0BFE-40E4-9712-37AF557F0BBB}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{1706EE68-BF3A-45F3-9933-BA2D257D4475}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{20542714-CE83-4D7E-9A4D-DF4354004719}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH3;0;ReadMethodType=bool;WriteMethodType=bool{22776981-36DB-40EF-8098-B787506C1DF6}resource=/crio_Chassis1_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=bool{2D65D439-4A25-47DE-BA19-0F1FCA327745}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{426078CC-340B-4FF7-A73D-57B7F8E8F71F}resource=/crio_Chassis1_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=bool{5F95C7FF-ABD4-43B2-BCB7-0083A4E9C3DD}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{69310FCA-4C28-4C90-A35E-BF723B8EA04B}resource=/crio_Chassis1_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=bool{695ACB1B-E4F1-4AF1-BCFE-DE97F757C6EE}resource=/crio_Chassis1_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=bool{6A817F8E-4E0C-4648-9DC3-D90D22CC3E34}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{70E85B13-D7B9-4CDF-B8FF-E939C6F4AA02}resource=/crio_Chassis1_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=bool{732EE144-32E3-45DB-B440-E01FBDACC102}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{7A1E51F5-D1D7-48D9-A99F-E5428CB746DF}resource=/crio_Chassis1_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=bool{9C0CD391-E157-4C01-AC2F-C8F92590AAC7}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9481,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{A5F7B6CE-8F02-4DA7-8CB0-91F64C97F905}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{B343A9D4-324D-4C7D-8BF7-93A8FC66D7D8}resource=/crio_Chassis1_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=bool{B9114A29-59DC-45A4-9EC4-8DB767CBCDAD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{BA6E6398-0533-4EA1-A6E8-82AE47999CF5}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{BE6563F0-A700-4608-9E01-45B5ECC859AF}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH0;0;ReadMethodType=bool;WriteMethodType=bool{C1C88408-17A2-4F2E-877A-C7D18BB6A02B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{CCF00DA0-1C48-4447-8D0F-5333142C9269}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{DAE836CD-371E-45F4-9C64-62626BE783D4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{DC490A36-E08A-44BA-B2CD-2A6295489E79}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{DE4E5E8F-7997-4C88-84AF-1502BB865925}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{DF05FEAA-C8A5-498F-99BA-B69CC19FFC87}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{E2DD37A9-4BB1-4387-81B9-E61C31488648}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{E5C92DBC-132E-4234-9043-7BC579F28B5E}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH2;0;ReadMethodType=bool;WriteMethodType=bool{E78A182D-B879-4D95-8163-162036538AF9}ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH1;0;ReadMethodType=bool;WriteMethodType=bool{F0C44365-0884-4C82-9C81-9E1010BDB396}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{FF2EBF26-B693-402E-9479-5A4F5A40A11F}resource=/crio_Chassis1_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis1_Mod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 1,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9481,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]Interlock_M01ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH0;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M02ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH1;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M03ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH2;0;ReadMethodType=bool;WriteMethodType=boolInterlock_M04ArbitrationForOutputData=NeverArbitrate;resource=/crio_Chassis1_Mod2/CH3;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16LVDT_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16LVDT_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16LVDT_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16LVDT_Flt_M01resource=/crio_Chassis1_Mod1/DO1;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M02resource=/crio_Chassis1_Mod1/DO3;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M03resource=/crio_Chassis1_Mod1/DO5;0;ReadMethodType=bool;WriteMethodType=boolLVDT_Flt_M04resource=/crio_Chassis1_Mod1/DO7;0;ReadMethodType=bool;WriteMethodType=boolLVDT_M01_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M02_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M03_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M04_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=100080000000000300094002000256310000094002000256320000144050000200000001094C5644545F4D656173000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_V1_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16LVDT_V1_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16LVDT_V1_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16LVDT_V1_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16LVDT_V2_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16LVDT_V2_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16LVDT_V2_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16LVDT_V2_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16OutOfLimits_M01resource=/crio_Chassis1_Mod1/DO0;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M02resource=/crio_Chassis1_Mod1/DO2;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M03resource=/crio_Chassis1_Mod1/DO4;0;ReadMethodType=bool;WriteMethodType=boolOutOfLimits_M04resource=/crio_Chassis1_Mod1/DO6;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_AFP\FPGA Bitfiles\RPCSAFP_LVDTCalc_fpgaLVDTTotem4po_lFooHHcyX4A.lvbitx</Property>
			</Item>
			<Item Name="Dependencies" Type="Dependencies">
				<Item Name="vi.lib" Type="Folder">
					<Item Name="FxpSim.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/FXPMathLib/sim/FxpSim.dll"/>
					<Item Name="LVFixedPointOverflowPolicyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVFixedPointOverflowPolicyTypeDef.ctl"/>
					<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				</Item>
			</Item>
			<Item Name="Build Specifications" Type="Build">
				<Item Name="fpga_LVDT_v2" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_LVDT_v2</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPCSAFP_LVDTCalc_fpgaLVDTv2_eC+9Bi5K1VA.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_AFP/FPGA Bitfiles/RPCSAFP_LVDTCalc_fpgaLVDTv2_eC+9Bi5K1VA.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPCSAFP_LVDTCalc_fpgaLVDTv2_eC+9Bi5K1VA.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_AFP/RPCS_AFP.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
					<Property Name="TargetName" Type="Str">LVDTCalc</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/LVDTCalc/fpga_LVDT_v2.vi</Property>
				</Item>
				<Item Name="fpga_LVDT_Totem_4_pots" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_LVDT_Totem_4_pots</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPCSAFP_LVDTCalc_fpgaLVDTTotem4po_pbHApeb4qOM.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_AFP/FPGA Bitfiles/RPCSAFP_LVDTCalc_fpgaLVDTTotem4po_pbHApeb4qOM.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPCSAFP_LVDTCalc_fpgaLVDTTotem4po_pbHApeb4qOM.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_AFP/RPCS_AFP.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
					<Property Name="TargetName" Type="Str">LVDTCalc</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/LVDTCalc/fpga_LVDT_Totem_4_pots.vi</Property>
				</Item>
				<Item Name="fpga_LVDT_Totem_4_pots_v2" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_LVDT_Totem_4_pots_v2</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPCSAFP_LVDTCalc_fpgaLVDTTotem4po_lFooHHcyX4A.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_AFP/FPGA Bitfiles/RPCSAFP_LVDTCalc_fpgaLVDTTotem4po_lFooHHcyX4A.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPCSAFP_LVDTCalc_fpgaLVDTTotem4po_lFooHHcyX4A.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_AFP/RPCS_AFP.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
					<Property Name="TargetName" Type="Str">LVDTCalc</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/LVDTCalc/fpga_LVDT_Totem_4_pots_v2.vi</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="AFP_TB_Ctrl_RT_V2.vi" Type="VI" URL="../AFP_TB_Ctrl_RT_V2.vi"/>
		<Item Name="2DArray.ctl" Type="VI" URL="../2DArray.ctl"/>
		<Item Name="UpdateCtrl.ctl" Type="VI" URL="../UpdateCtrl.ctl"/>
		<Item Name="AFP_MotorCtrl_RT.vi" Type="VI" URL="../AFP_MotorCtrl_RT.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="VISA Flush IO Buffer Mask.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Flush IO Buffer Mask.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="NI_Real-Time Target Support.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI_Real-Time Target Support.lvlib"/>
				<Item Name="ni_emb.dll" Type="Document" URL="/&lt;vilib&gt;/ni_emb.dll"/>
				<Item Name="Write To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File.vi"/>
				<Item Name="Write To Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (DBL).vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write To Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (I64).vi"/>
				<Item Name="Write To Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (string).vi"/>
			</Item>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Set_Current.vi" Type="VI" URL="../lib/Motor_Drive_RP/Set_Current.vi"/>
			<Item Name="Set_Resolution.vi" Type="VI" URL="../lib/Motor_Drive_RP/Set_Resolution.vi"/>
			<Item Name="Read_Status.vi" Type="VI" URL="../lib/Motor_Drive_RP/Read_Status.vi"/>
			<Item Name="RPState.ctl" Type="VI" URL="../fpga/Motor/RPState.ctl"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="AFPexe" Type="{69A947D5-514E-4E75-818E-69657C0547D8}">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{E78EB999-5807-4FD3-80C1-21BF796E661D}</Property>
				<Property Name="App_INI_GUID" Type="Str">{0D8B61FA-1C07-47BB-9E68-6277D1BE8356}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.cern.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{236BA12A-0F34-454C-B80B-273B27C3FD23}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">AFPexe</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/NI_AB_TARGETNAME/AFPexe</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{DC8C3D86-99FB-4195-B5F2-6680C19B962C}</Property>
				<Property Name="Bld_targetDestDir" Type="Path">/c/ni-rt/startup</Property>
				<Property Name="Bld_version.build" Type="Int">3</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">AFP_RP.rtexe</Property>
				<Property Name="Destination[0].path" Type="Path">/c/ni-rt/startup/AFP_RP.rtexe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/c/ni-rt/startup/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">system</Property>
				<Property Name="Destination[2].path" Type="Path">/c/ni-rt/system</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Source[0].itemID" Type="Str">{EFDDAA01-A26F-4794-9976-B7519A88A412}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[1].itemID" Type="Ref">/RT PXI Target/lib</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/RT PXI Target/lib/DIM/DLL/dim.dll</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/RT PXI Target/lib/DIM/DLL/TOTEM_DIM.dll</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/RT PXI Target/AFP_MotorCtrl_RT.vi</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">5</Property>
				<Property Name="TgtF_companyName" Type="Str">cern</Property>
				<Property Name="TgtF_enableDebugging" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">AFPexe</Property>
				<Property Name="TgtF_internalName" Type="Str">AFPexe</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2018 cern</Property>
				<Property Name="TgtF_productName" Type="Str">AFPexe</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{44A0DF10-9C87-4DF9-8294-E3572FBC2CD3}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">AFP_RP.rtexe</Property>
			</Item>
		</Item>
	</Item>
</Project>
