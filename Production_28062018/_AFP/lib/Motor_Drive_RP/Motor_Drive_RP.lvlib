﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Add_CRC_RP.vi" Type="VI" URL="../Add_CRC_RP.vi"/>
	<Item Name="Disable_Driver_RP.vi" Type="VI" URL="../Disable_Driver_RP.vi"/>
	<Item Name="Enable_Driver_RP.vi" Type="VI" URL="../Enable_Driver_RP.vi"/>
	<Item Name="Get_Driver_Index_RP.vi" Type="VI" URL="../Get_Driver_Index_RP.vi"/>
	<Item Name="Measure_Cable_Lenght_RP.vi" Type="VI" URL="../Measure_Cable_Lenght_RP.vi"/>
	<Item Name="Read_Cable_Lenght_RP.vi" Type="VI" URL="../Read_Cable_Lenght_RP.vi"/>
	<Item Name="Read_Custom_Register_RP.vi" Type="VI" URL="../Read_Custom_Register_RP.vi"/>
	<Item Name="Read_Position_RP.vi" Type="VI" URL="../Read_Position_RP.vi"/>
	<Item Name="Read_Register_RP.vi" Type="VI" URL="../Read_Register_RP.vi"/>
	<Item Name="Read_Status_RP.vi" Type="VI" URL="../Read_Status_RP.vi"/>
	<Item Name="Reset_Driver_RP.vi" Type="VI" URL="../Reset_Driver_RP.vi"/>
	<Item Name="Set_Cable_Lenght_RP.vi" Type="VI" URL="../Set_Cable_Lenght_RP.vi"/>
	<Item Name="Set_Current_RP.vi" Type="VI" URL="../Set_Current_RP.vi"/>
	<Item Name="Set_ResolutionRP.vi" Type="VI" URL="../Set_ResolutionRP.vi"/>
	<Item Name="Write_Custom_RegisterRP.vi" Type="VI" URL="../Write_Custom_RegisterRP.vi"/>
	<Item Name="Write_registersRP.vi" Type="VI" URL="../Write_registersRP.vi"/>
</Library>
