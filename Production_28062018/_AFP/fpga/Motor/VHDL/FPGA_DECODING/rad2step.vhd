----------------------------------------------------------------------------------
-- Company: 		CERN
-- Engineer: 		Arnaud BRIELMANN AB-ATB-LPE
-- 
-- Create Date:   	17:37:09 01/18/2007 
-- Design Name: 		resolver
-- Module Name:    	rad2step - Behavioral 
-- Project Name: 		resdec
-- Target Devices: 	xc2v3000-4fg676 on NI PXI-7833R board 
-- Tool versions: 	ISE & Core Generator v8.2.03i
-- Description: 		Output scaling radians to motor steps 
--							The output scale depend of the "res_factor" input
--							res_factor		scale
--							"00"				200 steps/rev
--							"01"				400 steps/rev
--							"10"				800 steps/rev
--							"11"				full precision (16384 steps/rev)
--
-- Dependencies: 		resolver.vhd
--							input_filters.vhd
--							fir_ls_bp_180_fir_compiler_v2_0_xst_1_mac_fir_v5_1_xst.edn
--							angle_logic.vhd
--							translate_scaled.ngc
--							angle_glitch_filter.vhd
--							shift_register_1bit.vhd
--							output_register.vhd
--							abs2rel.vhd
--							resolver.ucf
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity rad2step is
		Port (	clk 			: in  STD_LOGIC;
					rad_in 		: in  STD_LOGIC_VECTOR (31 downto 0);
					res_factor	: in 	STD_LOGIC_VECTOR (1 downto 0);--"00"=200step/rev;"01"=400;"10"=800;"11"=16384
					saturation	: out STD_LOGIC;
					step_out		: out STD_LOGIC_VECTOR (31 downto 0)
		);
end rad2step;

architecture Behavioral of rad2step is

signal rad_sig					: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal rad_sig_x4				: STD_LOGIC_VECTOR (38 downto 0) := (others => '0');
signal rad_sig_x4_tmp		: STD_LOGIC_VECTOR (33 downto 0) := (others => '0');
signal rad_sig_x32			: STD_LOGIC_VECTOR (38 downto 0) := (others => '0');
signal rad_sig_x32_tmp 		: STD_LOGIC_VECTOR (36 downto 0) := (others => '0');
signal rad_sig_x128			: STD_LOGIC_VECTOR (38 downto 0) := (others => '0');
signal rad_sig_x128_tmp		: STD_LOGIC_VECTOR (38 downto 0) := (others => '0');
signal step_sig				: STD_LOGIC_VECTOR (38 downto 0) := (others => '0');
signal step_sig_00_tmp		: STD_LOGIC_VECTOR (25 downto 0) := (others => '0');
signal step_sig_01_tmp		: STD_LOGIC_VECTOR (26 downto 0) := (others => '0');
signal step_sig_10_tmp		: STD_LOGIC_VECTOR (27 downto 0) := (others => '0');
signal step_sig_00			: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal step_sig_01			: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal step_sig_10			: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal step_sig_00_sat		: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal step_sig_01_sat 		: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal step_sig_10_sat		: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal saturation_sig		: STD_LOGIC := '0';
signal step_out_sig			: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal step_out_sig_sat		: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');

begin

rad_sig <= rad_in;

rad_sig_x4(1 downto 0) <= (others => '0');
rad_sig_x4(33 downto 2) <= rad_sig;
rad_sig_x4(38 downto 34) <= (others => rad_sig_x4(33));

rad_sig_x32(4 downto 0) <= (others => '0');
rad_sig_x32(36 downto 5) <= rad_sig;
rad_sig_x32(38 downto 37) <= (others => rad_sig_x32(36));

rad_sig_x128(6 downto 0) <= (others => '0');
rad_sig_x128(38 downto 7) <= rad_sig;

step_sig <= (rad_sig_x128 - rad_sig_x32) + rad_sig_x4; -- x100

step_sig_00(25 downto 0)	<= step_sig(38 downto 13) when step_sig(12)='0' 
										else 	step_sig(38 downto 13)+1;
step_sig_00(31 downto 26)	<= (others => step_sig_00(25));										

step_sig_01(26 downto 0)	<= step_sig(38 downto 12) when step_sig(11)='0' 
										else 	step_sig(38 downto 12)+1;
step_sig_01(31 downto 27)	<= (others => step_sig_01(26));										

step_sig_10(27 downto 0)	<= step_sig(38 downto 11) when step_sig(10)='0' 
										else 	step_sig(38 downto 11)+1;
step_sig_10(31 downto 28)	<= (others => step_sig_10(27));										

with res_factor select

	step_out_sig <= 	step_sig_00 	when "00",	-- 200 steps/rev 
							step_sig_01 	when "10",	-- 400 steps/rev  | LabView revert MSB <-> LSB
							step_sig_10 	when "01",	-- 800 steps/rev	| LabView revert MSB <-> LSB
							rad_sig 			when others;-- full precision (16384 steps/rev)

process(clk) begin
	if clk'event and clk='1' then
		if res_factor/="11" then
			if (step_out_sig(31)='0') and (step_out_sig(27 downto 15)/="0000000000000") then -- positive saturation
				step_out_sig_sat <= X"0000_7FFF";
				saturation_sig <= '1';
			elsif (step_out_sig(31)='1') and (step_out_sig(27 downto 15)/="1111111111111") then -- negative saturation
				step_out_sig_sat <= X"FFFF_8000";			
				saturation_sig <= '1';
			else
				step_out_sig_sat	<= step_out_sig;
				saturation_sig <= '0';
			end if;
		else
			step_out_sig_sat <= step_out_sig;
			saturation_sig <= '0';
		end if;
	end if;
end process;
step_out <= step_out_sig_sat;
saturation <= saturation_sig;
end Behavioral;

