----------------------------------------------------------------------------------
-- Company: 		CERN
-- Engineer: 		Arnaud BRIELMANN AB-ATB-LPE
-- 
-- Create Date:   	17:37:09 01/18/2007 
-- Design Name: 		resolver
-- Module Name:    	resolver - Behavioral 
-- Project Name: 		resdec
-- Target Devices: 	xc2v3000-4fg676 on NI PXI-7833R board 
-- Tool versions: 	ISE & Core Generator v8.2.03i
-- Description: 		Smart logic which eliminate the most extreme value of three
--							samples.
--
-- Dependencies: 		resolver.vhd
--							input_filters.vhd
--							fir_ls_bp_180_fir_compiler_v2_0_xst_1_mac_fir_v5_1_xst.edn
--							angle_logic.vhd
--							translate_scaled.ngc
--							shift_register_1bit.vhd
--							output_register.vhd
--							abs2rel.vhd
--							rad2step.vhd
--							resolver.ucf
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity angle_glitch_filter is
		Generic(	DATA_LENGTH		: positive);
		Port (	clk 				: in  STD_LOGIC;
					reset 			: in  STD_LOGIC;
					sample_clk 		: in  STD_LOGIC;
					data_in 			: in  STD_LOGIC_VECTOR (DATA_LENGTH-1 downto 0);
					data_out			: out  STD_LOGIC_VECTOR (DATA_LENGTH-1 downto 0)
					);
end angle_glitch_filter;

architecture Behavioral of angle_glitch_filter is

constant FA_WINDOW_SIZE : POSITIVE := 3;
constant FB_WINDOW_SIZE : POSITIVE := 2;
constant FC_WINDOW_SIZE : POSITIVE := 3;

type FA_Reg_array 	is array (FA_WINDOW_SIZE-1 downto 0) of STD_LOGIC_VECTOR(DATA_LENGTH downto 0);
type FB_Reg_array 	is array (FB_WINDOW_SIZE-1 downto 0) of STD_LOGIC_VECTOR(DATA_LENGTH downto 0);
type FC_Reg_array 	is array (FC_WINDOW_SIZE-1 downto 0) of STD_LOGIC_VECTOR(DATA_LENGTH downto 0);

signal fA_reg 		: FA_Reg_array;
signal fB_reg 		: FB_Reg_array;
signal fC_reg 		: FC_Reg_array;

--signals used for the first filter
signal fA_t20_d,fA_t21_d,fA_t10_d 				: STD_LOGIC_VECTOR(DATA_LENGTH downto 0);		
signal fA_t20_dn,fA_t21_dn,fA_t10_dn			: STD_LOGIC_VECTOR(DATA_LENGTH downto 0);		
signal fA_t20_abs,fA_t21_abs,fA_t10_abs 		: STD_LOGIC_VECTOR(DATA_LENGTH-1 downto 0);	
signal filterA_out									: STD_LOGIC_VECTOR(DATA_LENGTH-1 downto 0);	

--signals used for the second filter
signal fB_t20_d,fB_t21_d,fB_t10_d 				: STD_LOGIC_VECTOR(DATA_LENGTH downto 0);		
signal fB_t20_dn,fB_t21_dn,fB_t10_dn			: STD_LOGIC_VECTOR(DATA_LENGTH downto 0);		
signal fB_t20_abs,fB_t21_abs,fB_t10_abs 		: STD_LOGIC_VECTOR(DATA_LENGTH-1 downto 0);	
signal filterB_out									: STD_LOGIC_VECTOR(DATA_LENGTH-1 downto 0);	

--signals used for the second filter
signal fC_t20_d,fC_t21_d,fC_t10_d 				: STD_LOGIC_VECTOR(DATA_LENGTH downto 0);		
signal fC_t20_dn,fC_t21_dn,fC_t10_dn			: STD_LOGIC_VECTOR(DATA_LENGTH downto 0);		
signal fC_t20_abs,fC_t21_abs,fC_t10_abs 		: STD_LOGIC_VECTOR(DATA_LENGTH-1 downto 0);	
signal filterC_out									: STD_LOGIC_VECTOR(DATA_LENGTH-1 downto 0);	

begin
	
	process(sample_clk) begin
		if sample_clk'event and sample_clk='1' then
			--Shift first filter register
			for i in FA_WINDOW_SIZE-1 downto 1 loop
				fA_reg(i) 	<= fA_reg(i-1);
			end loop;
			--Shift second filter register
			for j in FB_WINDOW_SIZE-1 downto 1 loop
				fB_reg(j) 	<= fB_reg(j-1);
			end loop;
			--Shift filter C register
			for k in FC_WINDOW_SIZE-1 downto 1 loop
				fC_reg(k) 	<= fC_reg(k-1);
			end loop;
			fA_reg(0)(DATA_LENGTH-1 downto 0) <= data_in;
			--f2_reg(0)(DATA_LENGTH-1 downto 0) 	<= data_in; --does not work with signed values!
			fB_reg(0)(DATA_LENGTH-1 downto 0) <= filterA_out;
			fC_reg(0)(DATA_LENGTH-1 downto 0) <= fB_reg(FB_WINDOW_SIZE-1)(DATA_LENGTH-1 downto 0);
		end if;
	end process;

	-- Filter A
	fA_t20_d 	<= fA_reg(2) - fA_reg(0);
	fA_t21_d 	<= fA_reg(2) - fA_reg(1);
	fA_t10_d 	<= fA_reg(1) - fA_reg(0);
	fA_t20_dn	<= (not fA_t20_d)+1;
	fA_t21_dn 	<= (not fA_t21_d)+1;
	fA_t10_dn 	<= (not fA_t10_d)+1;
	fA_t20_abs 	<= fA_t20_d(DATA_LENGTH-1 downto 0) when fA_t20_d(DATA_LENGTH)='0' else fA_t20_dn(DATA_LENGTH-1 downto 0);
	fA_t21_abs 	<= fA_t21_d(DATA_LENGTH-1 downto 0) when fA_t21_d(DATA_LENGTH)='0' else fA_t21_dn(DATA_LENGTH-1 downto 0);
	fA_t10_abs 	<= fA_t10_d(DATA_LENGTH-1 downto 0) when fA_t10_d(DATA_LENGTH)='0' else fA_t10_dn(DATA_LENGTH-1 downto 0);
	filterA_out 	<= fA_reg(1)(DATA_LENGTH-1 downto 0) when ((fA_t10_abs<fA_t21_abs) and (fA_t10_abs<fA_t20_abs)) else fA_reg(2)(DATA_LENGTH-1 downto 0);

	-- Filter C
	fC_t20_d 	<= fC_reg(2) - fC_reg(0);
	fC_t21_d 	<= fC_reg(2) - fC_reg(1);
	fC_t10_d 	<= fC_reg(1) - fC_reg(0);
	fC_t20_dn	<= (not fC_t20_d)+1;
	fC_t21_dn 	<= (not fC_t21_d)+1;
	fC_t10_dn 	<= (not fC_t10_d)+1;
	fC_t20_abs 	<= fC_t20_d(DATA_LENGTH-1 downto 0) when fC_t20_d(DATA_LENGTH)='0' else fC_t20_dn(DATA_LENGTH-1 downto 0);
	fC_t21_abs 	<= fC_t21_d(DATA_LENGTH-1 downto 0) when fC_t21_d(DATA_LENGTH)='0' else fC_t21_dn(DATA_LENGTH-1 downto 0);
	fC_t10_abs 	<= fC_t10_d(DATA_LENGTH-1 downto 0) when fC_t10_d(DATA_LENGTH)='0' else fC_t10_dn(DATA_LENGTH-1 downto 0);
	filterC_out 	<= fC_reg(1)(DATA_LENGTH-1 downto 0) when ((fC_t10_abs<fC_t21_abs) and (fC_t10_abs<fC_t20_abs)) else fC_reg(2)(DATA_LENGTH-1 downto 0);
	
data_out	<= filterC_out;

end Behavioral;

