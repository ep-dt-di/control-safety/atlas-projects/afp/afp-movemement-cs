----------------------------------------------------------------------------------
-- Company: 		CERN
-- Engineer: 		Arnaud BRIELMANN AB-ATB-LPE
-- 
-- Create Date:   	17:37:09 01/18/2007 
-- Design Name: 		resolver
-- Module Name:    	resolver - Behavioral 
-- Project Name: 		resdec
-- Target Devices: 	xc2v3000-4fg676 on NI PXI-7833R board 
-- Tool versions: 	ISE & Core Generator v8.2.03i
-- Description: 		Angle computation using CORDIC Logicore for the arc-tangent
--							calculus. The instantaneous angle is given for a half turn.
--							Half turn transitions are detected and counted to generate
--							an absolute position over several turns. The result is
--							given in signed 32bits (13bits/half-turn and 10bits to
--							count half-turns)
--
-- Dependencies: 		resolver.vhd
--							input_filters.vhd
--							fir_ls_bp_180_fir_compiler_v2_0_xst_1_mac_fir_v5_1_xst.edn
--							translate_scaled.ngc
--							angle_glitch_filter.vhd
--							shift_register_1bit.vhd
--							output_register.vhd
--							abs2rel.vhd
--							rad2step.vhd
--							resolver.ucf
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity angle_logic is
	 Generic ( 	AVERAGE_SIZE			: positive := 4); 	-- Nb of samples for the magnitude averaging = 2^AVERAGE_SIZE
    Port ( 
				clk 							: in  	STD_LOGIC;
				reset 						: in  	STD_LOGIC;
				sample_clk 					: in  	STD_LOGIC;
				new_ref_in					: in  	STD_LOGIC;
				enable_in 					: in  	STD_LOGIC;
				enable_out 					: out  	STD_LOGIC;
				sensor_present				: out  	STD_LOGIC;
				new_ref_out					: out  	STD_LOGIC;
				sin 							: in  	STD_LOGIC_VECTOR (15 downto 0);
				cos 							: in  	STD_LOGIC_VECTOR (15 downto 0);
				threshold					: in		STD_LOGIC_VECTOR (15 downto 0);
				magnitude				:out STD_LOGIC_VECTOR (15 downto 0);
				instantaneous_position 	: out 	STD_LOGIC_VECTOR (31 downto 0)
			  );
end angle_logic;

architecture Behavioral of angle_logic is

component translate_scaled
	port (
			x_in		: IN 	std_logic_VECTOR(15 downto 0);
			y_in		: IN 	std_logic_VECTOR(15 downto 0);
			nd			: IN 	std_logic;
			phase_out: OUT std_logic_VECTOR(15 downto 0);
			x_out		: OUT std_logic_VECTOR(15 downto 0);
			rdy		: OUT std_logic;
			rfd		: OUT std_logic;
			clk		: IN 	std_logic
			);
end component;
constant CORDIC_RESOL			: POSITIVE := 14;

component angle_glitch_filter 
		Generic(	DATA_LENGTH		: positive);
		Port (	clk 				: in  STD_LOGIC;
					reset 			: in  STD_LOGIC;
					sample_clk 		: in  STD_LOGIC;
					data_in 			: in  STD_LOGIC_VECTOR (DATA_LENGTH-1 downto 0);
					data_out		 	: out  STD_LOGIC_VECTOR (DATA_LENGTH-1 downto 0)
					);
end component;

component shift_register_1bit
	Generic ( 	SIZE		: positive); 	
   Port ( 		clk 		: in  STD_LOGIC;
					data_in 	: in  STD_LOGIC;
					data_out : out  STD_LOGIC);
end component;

-- FPGA Express Black Box declaration
attribute fpga_dont_touch: string;
attribute fpga_dont_touch of translate_scaled: component is "true";
-- Synplicity black box declaration
attribute syn_black_box : boolean;
attribute syn_black_box of translate_scaled: component is true;


-------------------------------------------------------------------------------------
-- Signals used for the angle processing Logicore
-------------------------------------------------------------------------------------
signal sin_n, cos_n				: STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
signal sin_abs, cos_abs			: STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
signal magnitude_ok				: STD_LOGIC := '0';
signal cordic_out 				: STD_LOGIC_VECTOR (CORDIC_RESOL-2 downto 0) := (others => '0');
signal cordic_rdy					: STD_LOGIC := '0';
signal cordic_rfd 				: STD_LOGIC := '1';
signal cordic_nd					: STD_LOGIC := '0';
signal mag_inst					: STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
signal mag_tmp						: STD_LOGIC_VECTOR (AVERAGE_SIZE+15  downto 0) := (others => '0');
signal mag_average				: STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
signal mag_averaging_cnt		: STD_LOGIC_VECTOR (AVERAGE_SIZE downto 0) := (others => '0');
signal input_available			: STD_LOGIC := '0';

-------------------------------------------------------------------------------------
-- Signals used for filtering angle (half revolution)
-------------------------------------------------------------------------------------
signal angle_half					: STD_LOGIC_VECTOR (CORDIC_RESOL-2 downto 0) := (others => '0');
signal angle_half_prefiltered : STD_LOGIC_VECTOR (CORDIC_RESOL-2 downto 0) := (others => '0');
signal angle_half_filtered 	: STD_LOGIC_VECTOR (CORDIC_RESOL-2 downto 0) := (others => '0');

--------------------------------------------------------------------------------------
-- Signals used to detect transitions 0-PI -> PI-2PI
--------------------------------------------------------------------------------------
signal angle_half_t0				: STD_LOGIC_VECTOR (CORDIC_RESOL-2 downto 0) := (others => '0');
signal angle_half_t1				: STD_LOGIC_VECTOR (CORDIC_RESOL-2 downto 0) := (others => '0');
signal angle_half_t0_tmp		: STD_LOGIC_VECTOR (CORDIC_RESOL-1 downto 0) := (others => '0');
signal angle_half_t1_tmp		: STD_LOGIC_VECTOR (CORDIC_RESOL-1 downto 0) := (others => '0');
signal angle_half_t1t0dif		: STD_LOGIC_VECTOR (CORDIC_RESOL-1 downto 0) := (others => '0');
signal angle_half_t1t0dif_n	: STD_LOGIC_VECTOR (CORDIC_RESOL-1 downto 0) := (others => '0');
signal angle_half_t1t0abs		: STD_LOGIC_VECTOR (CORDIC_RESOL-2 downto 0) := (others => '0');

--------------------------------------------------------------------------------------
-- Signals used to generate absolute position counting half turns
--------------------------------------------------------------------------------------
signal pluscounting 				: STD_LOGIC := '0';
signal minuscounting 			: STD_LOGIC := '0';
signal halfturncounter			: STD_LOGIC_VECTOR (9 downto 0) := (others => '0');
signal absoluteposition			: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal cordic_out_notused 		: STD_LOGIC_VECTOR (15 downto CORDIC_RESOL-1);
signal new_ref_set				: STD_LOGIC;
signal new_ref_set_delayed		: STD_LOGIC;
signal new_ref_rst				: STD_LOGIC;
signal new_ref_out_sig			: STD_LOGIC;

begin

--------------------------------------------------------------------------------------
-- Angle processing using the CORDIC Logicore -----------------------------------
--------------------------------------------------------------------------------------
sin_n <= (not sin)+1;
sin_abs <= sin when sin(15)='0' else sin_n;
cos_n <= (not cos)+1;
cos_abs <= cos when cos(15)='0' else cos_n;
magnitude_ok <= '1' when (sin_abs>X"0400" or cos_abs>X"0400") else '0'; -- if both cos and sin are under 312.5mV the angle is not calculated

cordic_nd <= sample_clk and enable_in and cordic_rfd;
cordic_arctan_inst:translate_scaled
	port map(
			clk => clk, 
			phase_out(CORDIC_RESOL-2 downto 0) => cordic_out, -- the 2 MSB are not used in mode scaled radian
			phase_out(15 downto CORDIC_RESOL-1) => cordic_out_notused, 
			x_out => mag_inst, -- always positive => signed 18bits to unsigned 16bits
			y_in => sin,
			x_in => cos,
			nd => cordic_nd,
			rdy => cordic_rdy,
			rfd => cordic_rfd
			);

--------------------------------------------------------------------------------------
-- Average processing of the magnitude signal to determine if the input signal is available
--------------------------------------------------------------------------------------

process (sample_clk) begin
	if (sample_clk'event and sample_clk='1') then
		-- Average magnitude verification
		if mag_averaging_cnt(AVERAGE_SIZE) = '0' then
			mag_averaging_cnt <= mag_averaging_cnt + '1';
			mag_tmp <= mag_tmp + mag_inst;
		else
			mag_averaging_cnt <= (others => '0');
			mag_average <= mag_tmp(AVERAGE_SIZE+15 downto AVERAGE_SIZE);
			if mag_average<threshold then
				input_available <= '0' ;
			else
				input_available <= '1' ;
			end if;
			mag_tmp <= (others => '0');
		end if;
	end if;
end process;

angle_half <= cordic_out when (input_available='1' and cordic_rdy='1' and magnitude_ok='1');

--------------------------------------------------------------------------------------
-- Filter the signal to not take into account out of range values 
--------------------------------------------------------------------------------------

angle_half_filter:angle_glitch_filter
		generic map(DATA_LENGTH		=> 13)
		Port map (	
					clk 				=> clk,
					reset 			=> reset,
					sample_clk 		=> sample_clk,
					data_in 			=> angle_half,
					data_out			=> angle_half_filtered
					);


--angle_half_filtered <= angle_half;
-------------------------------------------------------------------------------------
-- Detect transitions 0-PI -> PI-2PI
-------------------------------------------------------------------------------------
	
process (sample_clk) begin
	if (sample_clk'event and sample_clk='1') then
		angle_half_t1 <= angle_half_t0;
		angle_half_t0 <= angle_half_filtered;
	end if;
end process;

angle_half_t0_tmp(CORDIC_RESOL-2 downto 0) <= angle_half_t0;
angle_half_t1_tmp(CORDIC_RESOL-2 downto 0) <= angle_half_t1;
angle_half_t1t0dif 	<= angle_half_t1_tmp - angle_half_t0_tmp;
angle_half_t1t0dif_n <= (not angle_half_t1t0dif)+1;
angle_half_t1t0abs 	<= angle_half_t1t0dif(CORDIC_RESOL-2 downto 0) when angle_half_t1t0dif(CORDIC_RESOL-1)='0' else
								angle_half_t1t0dif_n(CORDIC_RESOL-2 downto 0);
-------------------------------------------------------------------------------------
-- Generate absolute position counting half turns
-------------------------------------------------------------------------------------
process (clk) begin
	if clk'event and clk='1' then
		if new_ref_in='1' and new_ref_rst='0' then
			new_ref_set <= '1';
		end if;
		if new_ref_rst='1' then
			new_ref_set <= '0';
		end if;
	end if;
end process;

new_ref_delay: shift_register_1bit
generic map(	SIZE		=>	8)
port map(		clk 		=> sample_clk,
					data_in 	=> new_ref_set,
					data_out => new_ref_set_delayed
			);

process (sample_clk) begin
	if (sample_clk'event and sample_clk='1') then
		if new_ref_set='1' then
			new_ref_rst <= '1';
		end if;
		if reset='1' or new_ref_set_delayed='1' then
			halfturncounter <= (others=>'0');
			pluscounting <= '0';
			minuscounting <= '0';
			new_ref_out_sig <= '1';
		elsif (angle_half_t1t0abs(CORDIC_RESOL-2)='1' and input_available='1') then -- transition >= PI/2
			if (angle_half_t1t0dif(CORDIC_RESOL-1)='0' and pluscounting='0' and halfturncounter/="0111111111") then
				halfturncounter <= halfturncounter + 1;
				pluscounting <= '1';
				minuscounting <= '0';
			elsif (angle_half_t1t0dif(CORDIC_RESOL-1)='1' and minuscounting='0' and halfturncounter/="1000000000") then
				halfturncounter <= halfturncounter - 1;
				pluscounting <= '0';
				minuscounting <= '1';
			else
				pluscounting <= '0';
				minuscounting <= '0';
			end if;
			new_ref_rst <= '0';
			new_ref_out_sig <= '0';
		else
			pluscounting <= '0';
			minuscounting <= '0';
			new_ref_rst <= '0';
			new_ref_out_sig <= '0';
		end if;
		absoluteposition(CORDIC_RESOL-2 downto 0)  <= angle_half_t1;
		absoluteposition(CORDIC_RESOL+8 downto CORDIC_RESOL-1) <= halfturncounter;
		absoluteposition(31 downto CORDIC_RESOL+9) <= (others => halfturncounter(9));

	end if;
end process;

-----------------------------------------------------------------------
-- Assign outputs
-----------------------------------------------------------------------
sensor_present <= input_available;
new_ref_out <= new_ref_out_sig;
instantaneous_position <= absoluteposition;
enable_out <= enable_in;
magnitude <= mag_average;

end Behavioral;
