----------------------------------------------------------------------------------
-- Company: 		CERN
-- Engineer: 		Arnaud BRIELMANN AB-ATB-LPE
-- 
-- Create Date:   	17:37:09 01/18/2007 
-- Design Name: 		resolver
-- Module Name:    	resolver - Behavioral 
-- Project Name: 		resdec
-- Target Devices: 	xc2v3000-4fg676 on NI PXI-7833R board 
-- Tool versions: 	ISE & Core Generator v8.2.03i
-- Description: 		Compute an average of 2^WIDTH samples on arising edge of the 
--							"start" signal.
--
-- Dependencies: 		resolver.vhd
--							input_filters.vhd
--							fir_ls_bp_180_fir_compiler_v2_0_xst_1_mac_fir_v5_1_xst.edn
--							angle_logic.vhd
--							translate_scaled.ngc
--							angle_glitch_filter.vhd
--							shift_register_1bit.vhd
--							abs2rel.vhd
--							rad2step.vhd
--							resolver.ucf
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
-- Modification:	Sergio Batuca, 06/05/2009
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity output_register is
	 Generic ( 	WIDTH			: positive := 8; 		-- Average size = 2^WIDTH
					LATENCY		: positive := 200);	-- Latency of input filters [samples] 
    Port ( 	clk 				: in  	STD_LOGIC;
				reset				: in  	STD_LOGIC;
				sample_clk 		: in  	STD_LOGIC;
				enable_in 		: in  	STD_LOGIC;
				start 			: in  	STD_LOGIC;
				new_ref_in		: in		STD_LOGIC;
				position_in 	: in  	STD_LOGIC_VECTOR (31 downto 0);
				enable_out 		: out  	STD_LOGIC;
				strobe 			: out  	STD_LOGIC;
				isreading 		: out  	STD_LOGIC;
				new_ref_out		: out		STD_LOGIC;
				position_out 	: out  	STD_LOGIC_VECTOR (31 downto 0);
				debug1				: out		STD_LOGIC;
				debug2				: out		STD_LOGIC
				);
end output_register;

architecture Behavioral of output_register is

-- Signals used to delay the start signal in order to match the input filter latency
signal start_delayed				: STD_LOGIC := '0';
signal start_received			: STD_LOGIC := '0';
signal start_sig					: STD_LOGIC := '0';

-- Signals used to process the position average
signal reading_in_progress 	: STD_LOGIC := '0';
signal reading_finished			: STD_LOGIC := '0';
signal reading_finished_dly	: STD_LOGIC := '0';
signal sample_cnt					: STD_LOGIC_VECTOR (WIDTH downto 0) := (others=>'0');
signal position_accumul			: STD_LOGIC_VECTOR (31+WIDTH downto 0) := (others=>'0');
signal position_reg_in			: STD_LOGIC_VECTOR (31+WIDTH downto 0) := (others=>'0');
signal position_reg_out			: STD_LOGIC_VECTOR (31 downto 0) := (others=>'0');

-- Signals used for the new reference process
signal new_ref_done				: STD_LOGIC := '0';
signal new_ref_delayed			: STD_LOGIC := '0';
signal new_ref_rising			: STD_LOGIC := '0';
signal new_ref_delay_cnt		: STD_LOGIC_VECTOR (8 downto 0) := (others=>'0');
signal new_ref_out_sig			: STD_LOGIC := '0';
signal strobe_sig					: STD_LOGIC := '0';
signal strobe_sig_delayed		: STD_LOGIC := '0';

begin

start_sig <= start;-- or new_ref_done;

process(clk) begin
	if clk'event and clk='1' then
		-- Detect start signal
		if reading_in_progress='1' then
			start_received <= '0';
		elsif start_sig='1' and start_delayed='0' then
			start_received <= '1';
		end if;
		start_delayed <= start_sig;
	end if;
end process;

process(sample_clk) begin
	if (sample_clk'event and sample_clk='1') then
		-- Wait for the end of the reading before restart acquisition
		if reading_finished='1' and reading_in_progress='0' and start_received='0' then
			reading_in_progress <= '1';
		end if;
		-- Process the position average
		position_reg_in(31 downto 0) <= position_in;
		position_reg_in(31+WIDTH downto 32) <= (others=>position_in(31));
		if start_received='1' then
			reading_in_progress <= '1';
		end if;
		if sample_cnt(WIDTH)='1' then
			reading_finished <= '1';
			position_reg_out <= position_accumul(31+WIDTH downto WIDTH);
			position_accumul <= (others=>'0');
			sample_cnt <= (others=>'0');
			reading_in_progress <= '0';
		elsif reading_in_progress='1' then
			sample_cnt <= sample_cnt + 1;
			position_accumul <= position_accumul + position_reg_in;
			reading_finished <= '0';
		end if;

		-- Reset averaging when new_ref signal is recieved
		if	new_ref_rising='1' then
			position_accumul <= (others=>'0');
			sample_cnt <= (others=>'0');
			reading_in_progress <= '1';	
			new_ref_done <= '1';
			new_ref_out_sig <= '0';
		elsif new_ref_done='1' and reading_finished='1' and reading_finished_dly='0' then
			new_ref_done <= '0';
			new_ref_out_sig <= '1';
		else
			new_ref_out_sig <= '0';
		end if;
		
		new_ref_rising <= new_ref_in and not new_ref_delayed;
		new_ref_delayed <= new_ref_in;
		
		reading_finished_dly <= reading_finished;
		
		strobe_sig_delayed <= strobe_sig;
		strobe_sig <= reading_finished;
	end if;
end process;

-- Asign output signals
enable_out 		<= enable_in;
isreading 		<= reading_in_progress;
strobe 			<= strobe_sig_delayed;
position_out 	<= position_reg_out;
new_ref_out		<= new_ref_out_sig;

end Behavioral;
