----------------------------------------------------------------------------------
-- Company: 		CERN
-- Engineer: 		Arnaud BRIELMANN AB-ATB-LPE
-- 
-- Create Date:   	17:37:09 01/18/2007 
-- Design Name: 		resolver
-- Module Name:    	resolver - Behavioral 
-- Project Name: 		resdec
-- Target Devices: 	xc2v3000-4fg676 on NI PXI-7833R board 
-- Tool versions: 	ISE & Core Generator v8.2.03i
-- Description: 		Instantiate twice a FIR filter for noise immunity
--							the filter is designed in Matlab; parameters are (fir_ls_180_v2.fda):
--							- Direct Form FIR Bandpass Least-Square
--							- Order: 180
--							- Frequencies (kHz): Fs=200, Fstop1=6.5, Fpass1=7.1, Fpass2=7.9, Fstop2=8.5
--							- Wstop1=Wpass=Wstop2=1
--							- Coefficients fixed-point 16bits signed (fir_ls_180_v2.coe)
--
-- Dependencies: 		resolver.vhd
--							fir_ls_bp_180_fir_compiler_v2_0_xst_1_mac_fir_v5_1_xst.edn
--							angle_logic.vhd
--							translate_scaled.ngc
--							angle_glitch_filter.vhd
--							shift_register_1bit.vhd
--							output_register.vhd
--							abs2rel.vhd
--							rad2step.vhd
--							resolver.ucf
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity input_filters is
    Port ( 
				clk 			: in  STD_LOGIC;
				rst			: in  STD_LOGIC;
				sample_clk 	: in  STD_LOGIC;
				enable_in 	: in  STD_LOGIC;
				enable_out 	: out  STD_LOGIC;
				cos_in 	: in  STD_LOGIC_VECTOR (15 downto 0);
				sin_in 	: in  STD_LOGIC_VECTOR (15 downto 0);
				cos_out 	: out  STD_LOGIC_VECTOR (15 downto 0);
				sin_out 	: out  STD_LOGIC_VECTOR (15 downto 0)
			  );
end input_filters;

architecture Behavioral of input_filters is

component fir_ls_bp_180_fir_compiler_v2_0_xst_1_mac_fir_v5_1_xst
	port (
	CLK: IN std_logic;
	RESET: IN std_logic;
	ND: IN std_logic;
	RDY: OUT std_logic;
	RFD: OUT std_logic;
	DIN: IN std_logic_VECTOR(15 downto 0);
	DOUT: OUT std_logic_VECTOR(39 downto 0));
end component;

---------------------------------------------------------------------------------------------
-- Signals used for the MAC FIR Logicore
---------------------------------------------------------------------------------------------
signal fir_cos_out 							: STD_LOGIC_VECTOR (39 downto 0) := (others => '0');
signal fir_cos_rdy							: STD_LOGIC := '0';
signal fir_cos_rfd 							: STD_LOGIC := '1';
signal fir_cos_nd								: STD_LOGIC := '0';
signal fir_sin_out 							: STD_LOGIC_VECTOR (39 downto 0) := (others => '0');
signal fir_sin_rdy							: STD_LOGIC := '0';
signal fir_sin_rfd 							: STD_LOGIC := '1';
signal fir_sin_nd								: STD_LOGIC := '0';

begin

fir_cos_nd <= sample_clk and enable_in;
filter_cos : fir_ls_bp_180_fir_compiler_v2_0_xst_1_mac_fir_v5_1_xst
		port map (
			clk		=> clk,
			RESET 	=> rst,
			nd			=> fir_cos_nd,
			din		=> cos_in,
			rfd		=> fir_cos_rfd,
			rdy		=> fir_cos_rdy,
			dout		=> fir_cos_out
			);

fir_sin_nd <= sample_clk and enable_in;
filter_sin : fir_ls_bp_180_fir_compiler_v2_0_xst_1_mac_fir_v5_1_xst
		port map (
			clk		=> clk,
			RESET 	=> rst,
			nd			=> fir_sin_nd,
			din		=> sin_in,
			rfd		=> fir_sin_rfd,
			rdy		=> fir_sin_rdy,
			dout		=> fir_sin_out
			);
			
	
--------------------------------------------------------------------------------------------
-- Assign outputs
--------------------------------------------------------------------------------------------
enable_out <= enable_in;
cos_out <= fir_cos_out(36 downto 21);
sin_out <= fir_sin_out(36 downto 21);
			
end Behavioral;

