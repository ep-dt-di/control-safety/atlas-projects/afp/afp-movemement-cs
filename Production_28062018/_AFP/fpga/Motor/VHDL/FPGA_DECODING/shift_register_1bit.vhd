----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:28:19 07/03/2007 
-- Design Name: 
-- Module Name:    shift_register - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shift_register_1bit is
	Generic ( SIZE	: positive := 16); 	
   Port ( 	clk 		: in  STD_LOGIC;
				data_in 	: in  STD_LOGIC;
				data_out : out  STD_LOGIC);
end shift_register_1bit;

architecture Behavioral of shift_register_1bit is

signal reg0 : STD_LOGIC_VECTOR (SIZE-1 downto 0) := (others=>'0');

begin

process (clk) begin
	if clk'event and clk='1' then
		reg0 <= data_in & reg0(SIZE-1 downto 1);
	end if;
end process;

data_out 	<= reg0(0);

end Behavioral;

