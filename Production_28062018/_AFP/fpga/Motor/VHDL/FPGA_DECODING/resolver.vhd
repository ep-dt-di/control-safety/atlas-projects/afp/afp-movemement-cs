----------------------------------------------------------------------------------
-- Company: 		CERN
-- Engineer: 		Arnaud BRIELMANN AB-ATB-LPE
-- 
-- Create Date:   	17:37:09 01/18/2007 
-- Design Name: 		resolver
-- Module Name:    	resolver - Behavioral 
-- Project Name: 		resdec
-- Target Devices: 	xc2v3000-4fg676 on NI PXI-7833R board 
-- Tool versions: 	ISE & Core Generator v8.2.03i
-- Description: 		Resolver position decoding algorithm developped for LHC collimators
--							Excitation: 		7.5kHz
--							Sampling rate:		200kHz
--							Clock rate:			40MHz
--							ADC full scale:	+-10V/16bits (signed)
--
-- Dependencies: 		input_filters.vhd
--							fir_ls_bp_180_fir_compiler_v2_0_xst_1_mac_fir_v5_1_xst.edn
--							angle_logic.vhd
--							translate_scaled.ngc
--							angle_glitch_filter.vhd
--							shift_register_1bit.vhd
--							output_register.vhd
--							abs2rel.vhd
--							rad2step.vhd
--							resolver.ucf
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity resolver is
    Port ( 	clk 				: in  STD_LOGIC;
				reset 			: in  STD_LOGIC;
				enable_in 		: in  STD_LOGIC;		
				sample_clk 		: in  STD_LOGIC;
				start 			: in  STD_LOGIC;
				new_ref			: in  STD_LOGIC;
				res_factor		: in  STD_LOGIC_VECTOR (1 downto 0);
				cos 				: in  STD_LOGIC_VECTOR (15 downto 0);
				sin 				: in  STD_LOGIC_VECTOR (15 downto 0);
				threshold		: in  STD_LOGIC_VECTOR (15 downto 0);
				reference		: in  STD_LOGIC_VECTOR (31 downto 0);
				enable_out 		: out STD_LOGIC;
				strobe 			: out STD_LOGIC;
				sig_magnitude	: out STD_LOGIC_VECTOR (15 downto 0);
				sensor_present : out STD_LOGIC;
				saturation		: out STD_LOGIC;
				position_inst	: out STD_LOGIC_VECTOR (31 downto 0);
				position_abs	: out STD_LOGIC_VECTOR (31 downto 0);
				filtered_cos	: out STD_LOGIC_VECTOR (15 downto 0);
				filtered_sin	: out STD_LOGIC_VECTOR (15 downto 0);
				position_step	: out STD_LOGIC_VECTOR (31 downto 0)
			);
end resolver;

architecture Behavioral of resolver is

component input_filters
	Port ( 
		clk 			: in  STD_LOGIC;
		rst			: in  STD_LOGIC;
		sample_clk 	: in  STD_LOGIC;
		enable_in 	: in  STD_LOGIC;
		enable_out 	: out STD_LOGIC;
		cos_in 		: in  STD_LOGIC_VECTOR (15 downto 0);
		sin_in 		: in  STD_LOGIC_VECTOR (15 downto 0);
		cos_out 		: out STD_LOGIC_VECTOR (15 downto 0);
		sin_out 		: out STD_LOGIC_VECTOR (15 downto 0)
	);
end component;

component angle_logic
	Generic ( 	AVERAGE_SIZE	: positive); 	
	Port (
		clk 							: in  STD_LOGIC;
		reset 						: in  STD_LOGIC;
		sample_clk 					: in  STD_LOGIC;
		new_ref_in					: in  STD_LOGIC;
		enable_in 					: in  STD_LOGIC;
		enable_out 					: out STD_LOGIC;
		sensor_present				: out STD_LOGIC;
		new_ref_out					: out STD_LOGIC;
		cos 							: in  STD_LOGIC_VECTOR (15 downto 0);
		sin 							: in  STD_LOGIC_VECTOR (15 downto 0);
		magnitude 							: out  STD_LOGIC_VECTOR (15 downto 0);
		threshold					: in  STD_LOGIC_VECTOR (15 downto 0);
		instantaneous_position 	: out STD_LOGIC_VECTOR (31 downto 0)
	);
end component;

component output_register 
	Generic ( 	WIDTH 	: positive;
					LATENCY 	: positive);
   Port ( 
		clk 				: in  STD_LOGIC;
      reset 			: in  STD_LOGIC;
		sample_clk 		: in  STD_LOGIC;
      enable_in 		: in  STD_LOGIC;
      start 			: in  STD_LOGIC;
		new_ref_in		: in  STD_LOGIC;
      position_in 	: in  STD_LOGIC_VECTOR (31 downto 0);
      enable_out 		: out  STD_LOGIC;
      strobe 			: out  STD_LOGIC;
		isreading 		: out  STD_LOGIC;
		new_ref_out		: out  STD_LOGIC;
      position_out 	: out  STD_LOGIC_VECTOR (31 downto 0)
	);
end component;

component abs2rel
	port (
		clk 				: in  STD_LOGIC;
		reset 			: in  STD_LOGIC;
		sample_clk 		: in  STD_LOGIC;
		enable_in 		: in  STD_LOGIC;
		enable_out 		: out  STD_LOGIC;
		strobe_in		: in  STD_LOGIC;
		new_ref			: in  STD_LOGIC;
		data_in			: in  STD_LOGIC_VECTOR (31 downto 0);
		reference		: in  STD_LOGIC_VECTOR (31 downto 0);
		strobe_out		: out  STD_LOGIC;
		data_out			: out  STD_LOGIC_VECTOR (31 downto 0)
	);
end component;

component rad2step
	port (
		clk 				: in  STD_LOGIC;
		rad_in 			: in  STD_LOGIC_VECTOR (31 downto 0);
		res_factor		: in 	STD_LOGIC_VECTOR (1 downto 0);--"00"=200step/rev;"01"=400;"10"=800;"11"=16384
		saturation		: out	STD_LOGIC;
		step_out 		: out  STD_LOGIC_VECTOR (31 downto 0)
	);
end component;

signal sample_clk_sync				: STD_LOGIC := '0';

signal input_filters_en 			: STD_LOGIC := '0';
signal angle_logic_en 				: STD_LOGIC := '0';
signal output_register_en			: STD_LOGIC := '0';
signal filtered_sin_sig				: STD_LOGIC_VECTOR (15 downto 0);
signal filtered_cos_sig				: STD_LOGIC_VECTOR (15 downto 0);
signal signal_magnitude_sig 		: STD_LOGIC_VECTOR (15 downto 0);
signal position_inst_sig			: STD_LOGIC_VECTOR (31 downto 0);
signal position_rel_sig				: STD_LOGIC_VECTOR (31 downto 0);
signal position_abs_sig				: STD_LOGIC_VECTOR (31 downto 0);
signal position_step_sig			: STD_LOGIC_VECTOR (31 downto 0);
signal saturation_sig				: STD_LOGIC := '0';
signal reference_scaled				: STD_LOGIC_VECTOR (31 downto 0);
signal start_sig						: STD_LOGIC := '0';
signal strobe_sig						: STD_LOGIC := '0';
signal new_ref_angle_logic			: STD_LOGIC := '0';
signal new_ref_out_register		: STD_LOGIC := '0';

begin

input_filters_inst : input_filters
		port map (
			clk 			=> clk,
			rst			=> reset,
			sample_clk 	=> sample_clk,
			enable_in 	=> enable_in,
			enable_out 	=> input_filters_en,
			cos_in 		=> cos,
			sin_in 		=> sin,
			cos_out 		=> filtered_cos_sig,
			sin_out 		=> filtered_sin_sig
		);
			
angle_logic_inst : angle_logic
		generic map ( 	AVERAGE_SIZE	=> 4) 	
		port map (
			clk 							=> clk,
			reset							=> reset,
			sample_clk 					=> sample_clk,
			new_ref_in					=> new_ref,
			enable_in 					=> input_filters_en,
			enable_out					=> angle_logic_en,
			sensor_present				=> sensor_present,
			new_ref_out					=> new_ref_angle_logic,
			cos 							=> filtered_cos_sig,
			magnitude	=> signal_magnitude_sig,
			sin 							=> filtered_sin_sig,
			threshold					=> threshold,
			instantaneous_position	=> position_inst_sig
		);

output_register_inst : output_register 
		generic map ( 	WIDTH		=> 8,
							LATENCY 	=> 200)
		port map (
			clk 				=> clk,
			reset				=> reset,
         sample_clk 		=> sample_clk,
         enable_in 		=> angle_logic_en,
         start 			=> start,
			new_ref_in		=> new_ref_angle_logic,
         position_in 	=> position_inst_sig,
         enable_out 		=> output_register_en,
         strobe 			=> strobe_sig,
         new_ref_out		=> new_ref_out_register,
         position_out 	=> position_rel_sig
			);

abs2rel_inst : abs2rel
		port map(
			clk 				=> clk,		
			reset				=> reset,
			sample_clk 		=> sample_clk,		
			enable_in 		=> output_register_en,
			enable_out 		=> enable_out,		
			strobe_in		=> strobe_sig,		
			new_ref			=> new_ref_out_register,		
			data_in			=> position_rel_sig,		
			reference 		=> reference,
			strobe_out		=> strobe,		
			data_out			=> position_abs_sig		
		);

rad2step_inst : rad2step
		port map(
			clk 				=> clk,
			rad_in 			=> position_abs_sig,
			res_factor		=> res_factor,
			saturation		=> saturation_sig,
			step_out 		=> position_step_sig
		);

filtered_cos 	<= filtered_cos_sig;
filtered_sin 	<= filtered_sin_sig;
position_inst 	<= position_inst_sig;
position_abs 	<= position_abs_sig;
position_step 	<= position_step_sig;
saturation		<= saturation_sig;
sig_magnitude 	<= signal_magnitude_sig;


end Behavioral;

